% Part-based region matching implementation AKA Probabilistic Hough
% Transform
% Uses same API definition as author's code for compatibility
% Some parameter values set similar to author's implementation
% By Michael Smith
function [ confidenceMap ] = houghmatching_seg2( viewA, viewB, hsfilter )

% Get num regions in A + B
numRegionsA = size(viewA.frame,2);
numRegionsB = size(viewB.frame,2);

confidenceMapSize = [numRegionsA, numRegionsB];

% Tack on an extra row to allow for matrix computation
% Needed because of some preprocessing done earlier by the object discovery
% algorithm
bDescriptors2 = [viewB.desc; ones(1,size(viewB.desc,2))];

% Define p(m_a) as a similarity matrix
% This satisfies p(m_a) for all m from Eq. 3 in the paper
distance = double(viewA.desc')* double(bDescriptors2) ;
% Get rid of -ve values because they screw everything up
distance(distance < 0) = 0;


% Get X, Y, and scale values from all the regions in A
xValues = viewA.frame(1,:);
yValues = viewA.frame(2,:);
% scale = # pixels = area
sValues = viewA.frame(3,:) .* viewA.frame(6,:);

% NOTE: For frame variable
% 3 + 6 -> width, height of rectangle
% 1 = x coord, center
% 2 = y coord, center
% Other values unused (apparently)


% Get image width, height
width = viewB.imsize(1);
height = viewB.imsize(2);

% We need to discretize the potential offset regions into bins
% Say that we will have 5 pixel boxes + 20 scale divisions
% This is (somewhat) inspired by the values given by the authors in their
% version of the code

binDimensions = [width./5.0, height./5.0, 20];
posesX = linspace(1, width, binDimensions(1)+1);
posesY = linspace(1, height, binDimensions(2)+1);
% For scales we range from 0 to 4
% Note that we split the bins half between 0 and 1 and half between 1 and 4
% because a lot of scales end up < 1
firstPart = linspace(0,1,10+1);
secondPart = linspace(1,4,10+1);
posesZ = [firstPart, secondPart(2:end)];


% Track match-> match displacement
% e.g. reigon 5 in A to region 20 in B believes the displacement to be 2 in
% X, 10 in Y and 3 in scale
displacementX = zeros(confidenceMapSize);
displacementY = zeros(confidenceMapSize);
scaleZ = zeros(confidenceMapSize);

% Iterate over all regions in A, looking at one match there compared to
% *all* (as a vector, thank you MATLAB) regions in B
% We choose A because it will generally have fewer regions and thus less
% for loop iterations which we want to avoid (see sec. 3.3, part-based region
% matching in the paper)
% In this part our only job is to get the displacement (variable x in the
% paper) for all matches m given D sorted into bins
for regA = 1 : numRegionsA
    
    % In viewB.frame, we have the coordinates of all potential regions
    % Thus, we can calculate the displacement to use in m_g quickly using
    % vectorization
    % Here we are calculating the translation in X and Y
    xDiff = abs(viewB.frame(1,:) - xValues(regA));
    yDiff = abs(viewB.frame(2,:) - yValues(regA));
    % For scale, we need to calculate how big (# pixels) the square is vs.
    % the original
    scaleDiff = abs((viewB.frame(3,:) .* viewB.frame(6,:)) ./  sValues(regA));

    % Use vl_feat library to sort our differences into bins
    translationXBin = vl_binsearch(posesX, xDiff);
    translationYBin = vl_binsearch(posesY, yDiff);
    scaleChangeBin = vl_binsearch(posesZ, scaleDiff);
    
    
    % Remove any out of range bins (vl_binsearch may return some)
    translationXBin(translationXBin > binDimensions(1)) = binDimensions(1);
    translationYBin(translationYBin > binDimensions(2)) = binDimensions(2);
    scaleChangeBin(scaleChangeBin > binDimensions(3)) = binDimensions(3);
    
    translationXBin(translationXBin < 1) = 1;
    translationYBin(translationYBin < 1) = 1;
    scaleChangeBin(scaleChangeBin < 1) = 1;
    
    % Store the displacements/scale changes
    displacementX(regA,:) = translationXBin;
    displacementY(regA,:) = translationYBin;
    scaleZ(regA,:) = scaleChangeBin;
   
end

% Initialize our confidence map = p(m_a)
confidenceMap = distance;


% Now initialized to p(m_a) we calculate the rest of equation 3
% Unfortunately, calculating it properly would take *weeks* in MATLAB for a
% dataset like PASCAL VOC.  Instead, we make a key simplifying assumption. 
% The authors state that the geometry likelihood p(m_g | x) can be 
% calculated by comparing the displacement of m with the given offset x.  
% If we take that to mean that the probability
% is 1 when m has the same displacement as x and 0 otherwise, the
% sum over x ( p(m_g | x) * h(x | D) ) becomes 0 * h(x | D) when the x !=
% displacement of m and 1 * h(x | D) when x == displacement of m.  Thus,
% c(m|D) = p(m_a) * h(x |D) where x = displacement of m.  Using the same
% assumption, h(x|D) in turn becomes a sum of p(m_a) for all matches with
% some displacement x.  

% Thus, to calculate c(m|D) we need simply check all possible poses x, find
% all the matches that have said displacement and multiply them by 
% the sum of all p(m_a) with that displacement.  We do that below in 3 for
% loops.  Because I'm not technically comptetent enough to know how to find
% matches where all 3 (x,y,scale) poses match in a reasonable amount of time,
% I cheat: we do it for the three dimensions separately.  This in effect 
% will make matches where all 3 offsets match have the highest likelihoods,
% those with only 2 less so and only 1 even less.

% If these results are being compared against the presentation, the results
% are different because there was an error I have since fixed
% Calculate Probabilistic Hough Transform
for x = 1 : binDimensions(1)
   % Find all matches with a displacement matching this x
   index = displacementX == x;
   confidenceMap(index) = confidenceMap(index) .* sum(distance(index));
end

for y = 1 : binDimensions(2)
   % Find all matches with a displacement matching this y
   index = displacementY == y;
   confidenceMap(index) = confidenceMap(index) .* sum(distance(index));
end

for z = 1 : binDimensions(3)
   % Find all matches with a displacement matching this z
   index = scaleZ == z;
   confidenceMap(index) = confidenceMap(index) .* sum(distance(index));
end

% Apply Gaussian smoothing as indicated in paper
% This is kind of...quick and dirty compared to a proper Gaussian
% calcuation within the probability calculation but again: time constraints
% This code takes ~ 15 hours to run on the full dataset as it is
confidenceMap = convn(confidenceMap, hsfilter,'same');

end


