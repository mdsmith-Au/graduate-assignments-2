% Original author: Suha Kwak, Inria-Paris, WILLOW Project
% Modified by Michael Smith to be compatible with my new data format
function [ viewInfo ] = loadView_seg( id, folder )
% load image and associated information i.e. HoG features, bounding boxes


matFilePath = fullfile(folder, [id, '.mat']);

load( matFilePath, 'feat', 'gist_feat', 'bbox_list');


boxes = feat.boxes;

% suha - save image size separately
viewInfo.imsize = feat.imsize;  % width, height

bValid1 = boxes(:,1) > feat.imsize(1)*0.01 & boxes(:,3) < feat.imsize(1)*0.99 ...
        & boxes(:,2) > feat.imsize(2)*0.01 & boxes(:,4) < feat.imsize(2)*0.99;
bValid2 = boxes(:,1) < feat.imsize(1)*0.01 & boxes(:,3) > feat.imsize(1)*0.99 ...
        & boxes(:,2) < feat.imsize(2)*0.01 & boxes(:,4) > feat.imsize(2)*0.99;


idxValid = find(bValid1 | bValid2);
boxes = boxes(idxValid,:);
viewInfo.frame = box2frame(boxes');
viewInfo.type = ones(1,size(viewInfo.frame,2));
viewInfo.desc = cast(feat.hist(idxValid,:)', 'single');
% viewInfo.desc = full(feat.hist(idxValid,:)');
viewInfo.patch = cell(0);    
viewInfo.fileName = fullfile(folder, [id, '.jpg']);
viewInfo.bbox = [ 1, 1, feat.imsize(1), feat.imsize(2) ]';

% suha - save GIST as well
viewInfo.gist = gist_feat;


