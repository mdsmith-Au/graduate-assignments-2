\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{cvpr}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{comment}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{subcaption}
\usepackage{booktabs}
\usepackage[super]{nth}
\usepackage{float}

% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[breaklinks=true,bookmarks=false]{hyperref}

\cvprfinalcopy

\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}


\begin{document}

%%%%%%%%% TITLE
\title{Unsupervised Object Discovery and Localization in the Wild: Part-based Matching with Bottom-up Region Proposals}

\author{Michael Smith\\
McGill University\\
{\tt\small msmith@cim.mcgill.ca}}
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
% To save space, use either the email address or home page, not both
%\and
%Second Author\\
%Institution2\\
%First line of institution2 address\\
%{\tt\small secondauthor@i2.org}
%}

\maketitle
%\thispagestyle{empty}

%%%%%%%%% ABSTRACT
\begin{abstract}
	This paper addresses the problem of object discovery and localization by implementing and examining the work of Cho et al. as presented in ``Unsupervised Object Discovery and Localization in the Wild: Part-based Matching with Bottom-up Region Proposals'' at CVPR 2015.  We first review the problem of object detection and localization and the approach taken by Cho et al. followed by a discussion comparing the results of our implementation to theirs.  We discuss both qualitative and quantitative results and analyse the algorithm's strengths and weaknesses.
\end{abstract}

%%%%%%%%% BODY TEXT
\section{Introduction}
\paragraph{}
The problem of object detection and recognition is that of finding objects in an image and noting their specific location.  The paper ``Unsupervised Object Discovery and Localization in the Wild: Part-based Matching with Bottom-up Region Proposals''\cite{cho2015} addresses this problem as one where the goal is to draw a box outlining the dominant object in an image.  This is in similar yet different from segmentation, where the goal is to assign a label to every pixel in the image as opposed to denoting a bounding box.  Previous work has either relied on human annotation (which is expensive and may introduce bias) or fails to perform adequately when presented with challenging real-world images\cite{cho2015}.

\paragraph{}
The very first paper to apply probabilistic techniques to object detection was presented by Sivic \etal \cite{1541280} who was motivated by promising results achieved in fields such as speech recognition.  He applied the probabilistic Latent Semantic Analysis (pLSA) model through maximum likelihood estimation and expectation maximization to determine the category and then location of objects in images.  Russel \etal expanded\cite{1640948} on Sivic's work the following year, although the probabilistic portion of the algorithm remained essentially the same and results were comparable to that of Sivic's.  Grauman \etal \cite{1640737}  approached the problem in a different manner, treating it as a grouping problem with the only statistical technique used being PCA.  Faktor \etal\cite{6684535} defined a region comparison method that calculated the probability of one image being similar to another.  Through applications of maximum likelihood and Shannon's entropy, they achieved good results and defined the state of the art at the time.  Unfortunately, when faced with challenging datasets all of the above methods fail to produce acceptable results\cite{Deselaers2010,NIPS2009_3680,6909586}.  

\paragraph{}
A similar problem is cosegmentation, in which the goal is to segment a foreground object from an image\cite{cho2015}. Rother \etal\cite{1640859} were one of the first to tackle the problem, and did so using Markov Random Fields.  Other authors extended Rother's approach and made use of other statistical methods; examples include Batra \etal who used a GMM\cite{5540080} or Cho \etal who used Markov Chains and a Bayesian framework\cite{Cho2008}.  Colocolization, the task of denoting objects with bounding boxes, has also been approached in a statistical manner by Tan \etal \cite{6909586} through the use the discriminative clustering framework of Joulin \etal\cite{5539868}.

\section{Approach}
\paragraph{}
The approach as proposed by Cho \etal\cite{cho2015} can be split into two parts: part-based region matching and foreground localization.  A coordinate-descent style algorithm combines the two to find the appropriate localization of objects. The theory as presented by Cho \etal is summarized in this section.
\subsection{Part-based region matching}
\label{phm_sect}
\paragraph{}
\begin{figure*}[ht]
\centering
\begin{subfigure}[b]{.49\linewidth}
	\centering
	\includegraphics[width=.49\linewidth]{figs/img1.png}
	\includegraphics[width=.49\linewidth]{figs/img2.png}
	\caption{Original images.}
	\label{originalimg}
\end{subfigure}
\begin{subfigure}[b]{.49\linewidth}
	\centering	
	\includegraphics[width=.49\linewidth]{figs/region1.png}
	\includegraphics[width=.49\linewidth]{figs/region2.png}
	\caption{Region proposals for the respective images to the left.}
	\label{regionproposals}
\end{subfigure}

\begin{subfigure}[b]{\linewidth}
	\centering
	\includegraphics[width=.98\linewidth]{figs/regionmatch1.png}
	\caption{Region matches between the two images.}
	\label{regionmatches}
\end{subfigure}
\caption{Two images, their respective region proposals and region matches. From \cite{cho2015}.}
\end{figure*}

Part-based region matching begins by using what are known as `region proposals' extracted using the work of Manen \etal\cite{Manen_2013_ICCV} which annotates an image with numerous (generally a few thousand) bounding boxes that may or may not contain an object of interest. At least some of these regions will contain an important object and its parts, \eg a large box may contain a horse with smaller boxes containing a leg, the head, the tail, etc. Fig. \ref {regionproposals} shows an example of these proposals on two images, the originals of which are shown in Fig. \ref{originalimg}. With these two sets of region proposals $R$ and $R'$ in hand for two images $I$ and $I'$ respectively, Cho \etal\cite{cho2015} propose that it is possible to find a single region encompassing an object in an image by searching through all other images and comparing all of the potential regions to each other.  Specifically, they provide a probabilistic model representing the confidence that two regions are the same\cite{cho2015}:
\begin{equation}
\begin{split}
p(m|D) & = \sum_{x}p(m,x|D) = \sum_{x}p(m|x,D)p(x|D) \\
& = p(m_a)\sum_{x}p(m_g|x)p(x|D)
\end{split}
\label{eq1}
\end{equation}
We define $r = (f,l) \in R$ as a single region with $8$ X $8$ HOG features\cite{1467360} $f$ and location $l$ in the image.  A match is denoted $m = (r,r')$ where $r$ and $r'$ are two regions in images $I$ and $I'$ respectively. Fig. \ref{regionmatches} shows the twenty best matches between two images of a horse. There are $R \times R'$ matches for any two images while $D = (R,R')$ represents the set of all regions in both images. $x$ represents the pose displacement (translation and scale) required to map the foreground object from image $I$ to $I'$ irrespective of region proposals.  $p(m_a)$ is the appearance likelihood, defined as the similarity between HOG features for a given match while $p(m_g|x)$ is the geometry likelihood, calculated by comparing the displacement $l-l'$ of the two regions specified by $m$ to the given pose displacement $x$.

\paragraph{}
Unfortunately, the geometry prior $p(x|D)$ is impossible to calculate, and as such the authors propose a heuristic based on the Hough transform\cite{cho2015}:
\begin{equation}
\begin{split}
h(x|D) & = \sum_{m}p(m|x,D)\\
& = \sum_{m}p(m_a)p(m_g|x)
\end{split}
\label{eq2}
\end{equation}
With this heuristic, we can now rewrite Eq. \ref{eq1} and define the Hough match confidence\cite{cho2015}:
\begin{equation}
c(m|D) = p(m_a)\sum_{x}p(m_g|x)h(x|D)
\label{eq3}
\end{equation}
\paragraph{}
The authors name this \textit{Probabilistic Hough Matching} (PHM) and note that Eq. \ref{eq3} can be seen as a top-down process that evaluates the match confidence while Eq. \ref{eq2} can be seen as a bottom-up process intuitively described as one where regions with the same displacement from one image to the next (\ie the regions containing a foreground object and its parts) `vote' for each other, increasing their likelihood.  Finally, as with many probabilistic models the best match can be found via maximization\cite{cho2015}:
\begin{equation}
\phi(r) = \max_{r'}c((r,r')|(R,R'))
\end{equation}
With the best match, we also have the best region and thus a bounding box surrounding the object.
\subsection{Foreground Localization}
Unfortunately, part-based matching is not entirely sufficient to produce acceptable results.  The authors note that often the best matches are in fact parts of objects rather than entire objects as a whole\cite{cho2015}.  As such, foreground localization was introduced to correctly identify the whole object rather than its parts.  The authors based their technique on the Gestalt principles of visual perception\cite{rubinfigure} and design\cite{jackson2008gestalt}, in which a region that ``stands out'' is more likely to be foreground than background.  In other words, there is a high degree of contrast between a region containing a foreground object and one that contains more or less than the entirety of that object\cite{cho2015}.  The authors evaluate this contrast measure based on the region confidence acquired in the previous part-based matching step.  The procedure involves comparing a given region $r$ with both smaller regions contained within $r$ and larger regions that themselves contain $r$.  The correct region that contains the whole object can be found when there is a large difference in region confidence between $r$ and the larger region and only a small difference between $r$ and the smaller region.  For further details, please see \cite{cho2015}.
\subsection{Object discovery algorithm}
Object discovery combines the two previous steps to produce the final detection and localization of objects.  The authors employ a method similar to coordinate descent; they execute each part independently of the others. Algorithm \ref{algo1} illustrates the general procedure. Note that PHM stands for Probabilistic Hough Matching as described in Section \ref{phm_sect}.
\begin{algorithm}
\caption{Object Discovery\cite{cho2015}}
\label{algo1}
\begin{algorithmic}
\STATE{Get potential object regions for all images using \cite{Manen_2013_ICCV}}
\FOR{iteration = 1 \TO 5}
	\FOR{each image}
		\IF{iteration = 1} 	
			\STATE{Get nearest neighbour images using GIST\cite{4587633}}
		\ELSE 
			\STATE{Get nearest neighbour images using PHM}
		\ENDIF
	\ENDFOR
	\FOR{each image}
		\STATE{Get region confidence via PHM using nearest neighbours}
	\ENDFOR
	
	\FOR{each image}
		\STATE{Apply foreground localization}
	\ENDFOR
\ENDFOR
\end{algorithmic}
\end{algorithm}
\section{Results and Discussion}
\subsection{Experimental Procedure}
\paragraph{}
We chose to run experiments on two chalenging datasets: PASCAL VOC 2007\cite{pascal-voc-2007} and 2012\cite{pascal-voc-2012}, which contain thousands of images spread across twenty different classes.  All images used are annotated by humans with bounding boxes surrounding an instance of the appropriate class.  We chose to use the same six classes as Cho \etal on the same `train+val' set.\footnote{The full set of twenty classes would have required two weeks of processing power and as such was not feasible.}  Following the lead of Cho \etal, we used the CorLoc score which is a standard metric for bounding box localization problems.  It ``is defined as the percentage of images correctly localized according to the PASCAL criterion: $\frac{area(b_p \cap b_{gt})}{area(b_p \cup b_{gt})}$ where $b_p$ is the predicted box and $b_{gt}$ is the ground-truth box [annotated by humans].''\cite{cho2015}  For both datasets, we ran experiments using our implementation and that provided by Cho \etal with and without pruning, which removes any images labelled `difficult' or `truncated' in the dataset (neither the dataset authors\cite{pascal-voc-2007} nor \cite{cho2015} consider `difficult' images when evaluating performance). Finally, we chose to let the algorithm run for ten iterations so as to be able to confirm Cho \etal's observations that five iterations were sufficient\cite{cho2015}.
\subsection{Quantitative Results}
\begin{table*}
\centering
\caption{CorLoc scores on different datasets and implementations}
\begin{tabular}{cccccccccc}  
    \toprule
    \multicolumn{3}{c}{} & \multicolumn{6}{c}{Score by Class}\\
    \cmidrule{4-9}
    Dataset & Implementation & Pruning & Aeroplane & Bike & Boat & Bus & Horse & Motorbike & Average\\
	\midrule    
	2007 & Ours & Yes & 0.39 & 0.29 & 0.26 & 0.52 & 0.43 & 0.51 & 0.4\\
	2012 & Ours & Yes & 0.56 & 0.47 & 0.32 & 0.61 & 0.49 & 0.53 & 0.5\\
	2007 & Ours & No & 0.41 & 0.21 & 0.27 & 0.36 & 0.39 & 0.44 & 0.34\\
	2012 & Ours & No & 0.45 & 0.28 & 0.27 & 0.6 & 0.41 & 0.46 & 0.41\\
	2007 & Author & Yes & 0.47 & 0.5 & 0.4 & 0.69 & 0.78 & 0.6 & 0.57\\
	2012 & Author & Yes & 0.63 & 0.49 & 0.42 & 0.8 & 0.7 & 0.49 & 0.59\\
	2007 & Author & No & 0.41 & 0.37 & 0.27 & 0.44 & 0.56 & 0.42 & 0.41\\
	2012 & Author & No & 0.48 & 0.31 & 0.29 & 0.74 & 0.51 & 0.38 & 0.45\\
    \bottomrule
\end{tabular}
\label{corLoc}
\end{table*}
Table \ref{corLoc} shows the CorLoc scores calculated across both PASCAL datasets.  The authors' implementation as presented here either matches or exceeds the performance they report in \cite{cho2015} as a result of the choice of images to prune.  The authors remove any images that contain \textit{only} `difficult' or `truncated' objects (for their PASCAL07-all experiment), while we remove images that contain a \textit{single} `truncated' or `difficult' object.  Despite this difference, when executed on the exact same images we see that the author's implementation consistently outperforms ours by a significant margin, regardless of the dataset.  This can likely be explained by a combination of three different factors.  First, we discovered that the algorithm cannot be implemented as proposed by the authors without some approximations to allow the algorithm to execute in a reasonable amount of time (without such assumptions, runtime would be on the order of months).  For our implementation, we assume that $p(m_g|x) = 1$ if and only if $x$ has the same value as the displacement of $m$.  This means that we can simplify Eq. \ref{eq3} to:
\begin{equation}
c(m|D) = p(m_a)h(x|D), x = l - l'
\end{equation}
With the same assumption, we can also simplify Eq. \ref{eq2}:
\begin{equation}
h(x|D) = \sum_{m}p(m_a) \: \forall \:m \: s.t. \:x = l - l'
\end{equation}
While effective at reducing runtime, it is a given that these assumptions introduce error into the calculation.  Examination of the authors' code reveals that they use an approximation although it is unclear exactly what that approximation is.  Second, while the foreground localization step is intended to ensure that a bounding box is correctly localized to an entire object (and not part of one), on occasion it does not appear to perform as expected.  It is possible that parameters set by the authors for that step must be adjusted to compensate for our different part-based matching implementation.  Finally, we note that the authors make extensive use of non-probabilistic techniques such as removing regions based on an aspect ratio threshold to improve their results.  As we do not employ such techniques, it is to be expected that they achieve better results.
\paragraph{}
All of that said, Table \ref{corLoc} also reveals that the 2012 dataset improves on the results in both implementations.  The cause of this is likely due to the increase in the number of images from 2007 to 2012, which allows the matching process to find better nearest neighbour images and thus better region matches in general.  Lastly, we note that pruning (removing more challenging images) improves performance significantly for all implementations and datasets as one might expect.
\begin{figure*}[ht]
\centering
\begin{subfigure}[b]{.49\linewidth}
	\centering
	\includegraphics[width=\linewidth]{figs/graph_iter_me.png}
	\caption{CorLoc score for our implementation.}
	\label{graph_me}
\end{subfigure}
\begin{subfigure}[b]{.49\linewidth}
	\centering
	\includegraphics[width=\linewidth]{figs/graph_iter_author.png}
	\caption{CorLoc score for the authors' implementation.}
	\label{graph_author}
\end{subfigure}
\caption{A comparison of the CorLoc score as a function of the number of iterations between our implementation and the authors'.}
\label{graph_corloc}
\end{figure*}
\paragraph{}
Fig. \ref{graph_corloc} shows the CorLoc score as a function of the number of iterations of the algorithm.  Fig. \ref{graph_me} shows our implementation while Fig. \ref{graph_author} shows the authors' implementation.  We note that the authors' results generally improve as the number of iterations increases, with the score mostly unchanged after only a few iterations and thus confirming their belief that five iterations are sufficient.  By contrast, our implementation is not nearly as consistent and if anything performs worse as the number of iterations increases.  The cause of this discrepancy is unclear but is likely related to the same factors discussed earlier that result in the lower CorLoc score achieved by our implementation.
\label{quantitative}
\subsection{Qualitative Results}
\begin{figure*}[ht]
\centering
\begin{subfigure}[b]{.23\linewidth}
	\centering
	\includegraphics[width=\linewidth]{figs/qual/boat_059.jpg}
	\caption{Original image.}
	\label{boat_059}
\end{subfigure}
\begin{subfigure}[b]{.23\linewidth}
	\centering
	\includegraphics[width=\linewidth]{figs/qual/boat_059_i01_con.jpg}
	\caption{Region confidence, \nth{1} iter.}
	\label{boat_059_first}
\end{subfigure}
\begin{subfigure}[b]{.23\linewidth}
	\centering
	\includegraphics[width=\linewidth]{figs/qual/boat_059_i02_con.jpg}
	\caption{Region confidence, \nth{2} iter.}
	\label{boat_059_second}
\end{subfigure}
\begin{subfigure}[b]{.23\linewidth}
	\centering
	\includegraphics[width=\linewidth]{figs/qual/boat_059_i03_con.jpg}
	\caption{Region confidence, \nth{3} iter.}
	\label{boat_059_third}
\end{subfigure}
\caption{Region confidences across iterations for our implementation.  Only the first three iterations are shown here.}
\end{figure*}
\paragraph{}
Fig. \ref{boat_059} shows an image from the dataset with a boat, while Figs. \ref{boat_059_first} through \ref{boat_059_third} show the region confidences of that image as a function of the number of iterations for our implementation.  We see that as the algorithm progresses, we narrow our search for the correct region: in the first iteration (Fig. \ref{boat_059_first}), there is little difference in confidence between a region containing the boat alone and a region containing the boat and the beach.  In the second iteration (Fig. \ref{boat_059_second}), the confidence for the boat region increases dramatically, although the same can be said for some small erroneous regions at the top of the image.  In the third iteration (Fig. \ref{boat_059_third}), we see that the number of regions with high confidence has been significantly reduced to a few containing only the boat.  This shows the object discovery algorithm at work, as we see each iteration move closer to the optimal bounding box solution.  Unfortunately, it is important to note that these qualitative results are to some extent in conflict with the CorLoc scores shown in Fig. \ref{graph_corloc} which decrease as the number of iterations increases.  This is likely due to the way that the CorLoc score is calculated, as factors that a human may consider important such as the location of the box center are not taken into account.  For example, a class containing several images with a bounding box spanning all pixels in the image may have a better CorLoc score than one where the bounding boxes are smaller if the objects typically occupy a large portion of each image.
\begin{figure*}[ht]
\centering
\begin{subfigure}[b]{\linewidth}
	\centering
	\includegraphics[width=.3\linewidth]{figs/qualitative_me/aeroplane_002_res.jpg}
	\includegraphics[width=.3\linewidth]{figs/qualitative_me/bus_026_res.jpg}
	\includegraphics[width=.3\linewidth]{figs/qualitative_me/horse_043_res.jpg}
	\caption{Our results.}
	\label{our_qualitative_results}
\end{subfigure}
\begin{subfigure}[b]{\linewidth}
	\centering
	\includegraphics[width=.3\linewidth]{figs/qualitative_author/aeroplane_002_res.jpg}
	\includegraphics[width=.3\linewidth]{figs/qualitative_author/bus_026_res.jpg}
	\includegraphics[width=.3\linewidth]{figs/qualitative_author/horse_043_res.jpg}
	\caption{Authors' results.}
	\label{author_qualitative_results}
\end{subfigure}
\caption{A qualitative comparison of bounding box results for three images between our implementation and that of the authors.  Note that the red boxes indicate the result of the algorithm while the white boxes indicate ground truth as provided by the dataset.}
\end{figure*}
\paragraph{}
Fig. \ref{our_qualitative_results} shows a selection of three images and the bounding boxes our algorithm assigned.  Fig. \ref{author_qualitative_results} by comparison shows the results for the authors' implementation on the same images.  We see that for simple cases such as an aeroplane in the sky, both the authors' implementation and our own perform very well.  In other cases, however, such as the red double-decker bus or the horse the different implementations perform differently: our implementation succeeds in recognizing a greater portion of the bus while the authors' implementation manages to find a tighter bounding box around the horse.  Once again, the cause of such discrepancies is likely to be the same as discussed in Section \ref{quantitative}.
\subsection{General observations}
\paragraph{}
The quantitative and qualitative results show that in most situations the algorithm performs quite well and succeeds at identifying objects of interest in images.  This is made possible by the fact that most images in the dataset have a clear foreground/background separation, a scenario which caters to the algorithm's foreground localization step and can be considered one of its strengths.  That said, it is important to realize the step takes for granted that all images have a single foreground object that can be easily distinguished from a background.  Unfortunately, this is not always the case, as seen in Fig. \ref{fail_us} where we see both the authors' implementation and our own fail.  In the left image of the waterway, foreground localization is unable to compare a region containing the boat to one containing only a part of the boat as the region occupies only a small fraction of the image. The image of the motorcycle is an example of the other extreme, where there is no background for foreground localization to compare the motorcycle against.  As such, we can consider the foreground localization step to be both a strength and a potential weakness should certain key assumptions be violated.
\begin{figure*}[ht]
\centering
\begin{subfigure}[b]{.49\linewidth}
	\centering
	\includegraphics[height=2.75cm]{figs/fail_me/boat_045_res.jpg}
	\includegraphics[height=2.75cm]{figs/fail_me/motorbike_006_res.jpg}
	\label{fail_me}
	\caption{Our implementation.}
\end{subfigure}
\begin{subfigure}[b]{.49\linewidth}
	\centering
	\includegraphics[height=2.75cm]{figs/fail_author/boat_045_res.jpg}
	\includegraphics[height=2.75cm]{figs/fail_author/motorbike_006_res.jpg}
	\label{fail_author}
	\caption{Authors' implementation.}
\end{subfigure}
\caption{Examples of images where the assumptions made in the foreground localization step are violated and both implementations fail. Red boxes indicate the result of the algorithm; white boxes indicate ground truth as provided by the dataset.}
\label{fail_us}
\end{figure*}
\paragraph{}
Despite this problem, it is worth noting that compared to the state of the art the work of Cho \etal performs quite well.  The authors provide a comparison in their paper\cite{cho2015} and note that while their algorithm is outperformed by the state of the art such as \cite{Wang2014} such approaches make use of significantly more training data than this probabilistic method.  Considering the limited data this probabilistic method uses, it's performance is quite impressive.
\section{Conclusion}
\paragraph{}
In conclusion, we have provided a brief overview of the algorithm as presented in \cite{cho2015} and compared our implementation to that of the original authors.  We have shown that while our implementation is unfortunately not capable of achieving the same quantitative results as the authors demonstrate, it does achieve respectable qualitative results while benefiting and suffering from the same strengths and weaknesses.  As a consequence, we have shown that the underlying probabilistic approach is sound.  Future work should focus on improving the foreground localization step, as it is the algorithm's main weakness.  Improvements to that step could likely allow for the detection of multiple objects in a single image as well as enhanced detection of objects for which the foreground/background separation is unclear.
\section{Acknowledgements}
\paragraph{}
This implementation of Cho \etal's algorithm\cite{cho2015} was made possibly thanks to code provided by the authors.  The key probabilistic contribution of the paper, \textit{Part-based region matching} was written by us.  The authors' implementation was used for the foreground localization step and the overarching object discovery algorithm. We also wrote a custom function to load data from any PASCAL VOC dataset from 2007-2012 and refactored most of the authors' code to compensate for the new data format.  Library code used includes VLFeat\cite{vedaldi08vlfeat}, Progress monitor\cite{progressbar}, the PASCAL VOC dataset\cite{pascal-voc-2007,pascal-voc-2012} dev kits, Prime Object Proposals with Randomized Prim's Algorithm\cite{Manen_2013_ICCV} and Discriminatively trained deformable part models\cite{voc-release5,lsvm-pami}. For more information, please see the included MATLAB files; all files which we wrote or modified are indicated as such by a comment at the top of the file.  Files for which this is not indicated were not modified.
{\small
\bibliographystyle{ieee}
\bibliography{biblio}
}

\end{document}
