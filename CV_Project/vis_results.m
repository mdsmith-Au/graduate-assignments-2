% Original author: Suha Kwak, Inria-Paris, WILLOW Project
% Modified by Michael Smith to be compatible with my new data format
function vis_results(name, resultsFolder, imageListFile, num_max_iteration)


% =========================================================================
% PRELIMINARY

% Load in image IDs
try
    load(imageListFile);
    classes = imagesByClass.keys;
    nclass = length(classes);
catch
    error('Unable to load data; please run localization.');
end

resultsDir_loc = fullfile(resultsFolder, 'localization');

nimage = sum(cellfun('length',imagesByClass.values));

% parameters
nMaxBBoxDisp = 5;				% max # displayed bounding boxes


% path to the images and webpages
webp_path = fullfile(resultsFolder, 'Webpage');
if ~exist(webp_path, 'dir')
    mkdir(webp_path);
end

imgs_path = fullfile(webp_path, 'img');
if ~exist(imgs_path, 'dir')
    mkdir(imgs_path);
end

% =========================================================================
% GENERATE IMAGES

currentImageNum = 1;

for cidx = 1 : length(classes)
    cls_name = classes{cidx};
    
    imageIdList = imagesByClass(cls_name);
    for iidx = 1 : length(imagesByClass(cls_name))
        
        fprintf('Writing image %i/%i for class %s\n', currentImageNum, nimage, cls_name);
        currentImageNum = currentImageNum + 1;
        
        imageId = imageIdList{iidx};
        
        % load image/boxes ('feat.img', 'feat.boxes')
        idata   = loadView_seg(imageId, fullfile(resultsFolder, cls_name));
        
        % Bounding boxes
        bboxes  = frame2box(idata.frame)';
        % Original image
        img_org = imread(idata.fileName);
        
        % load ground-truth bounding boxes ("bbox_list")
        load(fullfile(resultsFolder, cls_name, [imageId, '.mat']));
        gt_boxes = cell2mat(bbox_list');
        ngtboxes = size(gt_boxes, 1);
        
        % write original image file
        img_file_path = fullfile(imgs_path, sprintf('%s_%03d.jpg', cls_name, iidx));
        imwrite(img_org, img_file_path, 'jpg');
        
        for itr = 1 : num_max_iteration
            
            % load localization results ('saliency', 'conf_acc')
            load(fullfile(resultsFolder,'localization',cls_name, sprintf('sai%03d_i%02d.mat', iidx, itr)) );
            
            % dense confidence image --------------------------------------------------
            % sorting raw confidences
            [saliv, ranki] = sort(conf_acc);
            
            % set colors
            cmap = vals2colormap(saliv, 'jet');
            cmap = ceil(cmap .* 255);
            
            % generate result images
            img_con = repmat(rgb2gray(img_org), [1, 1, 3]);
            for bidx = 1 : length(ranki)
                bbox = bboxes(ranki(bidx), :);
                img_con = drawBox(img_con, bbox, 2, cmap(bidx, :));
            end
            img_con_path = fullfile(imgs_path, sprintf('%s_%03d_i%02d_con.jpg', cls_name, iidx, itr));
            imwrite(img_con, img_con_path, 'jpg');
            
            
            % top-K bounding boxes only -----------------------------------------------
            % sorting and selecting few top elements
            [ ranki, saliv ] = select_kbestbox(bboxes', saliency, nMaxBBoxDisp);

            % set colors
            if isempty(ranki)
                img_box = repmat(rgb2gray(img_org), [1, 1, 3]);
            else
                cmap = vals2colormap(saliv, 'jet');
                cmap = ceil(cmap .* 255);
                
                % generate result images
                img_box = repmat(rgb2gray(img_org), [1, 1, 3]);
                for gidx = 1 : ngtboxes
                    bbox = gt_boxes(gidx, :);
                    img_box = drawBox(img_box, bbox, 4, [0, 0, 0]);
                    img_box = drawBox(img_box, bbox, 2, [255, 255, 255]);
                end
                for bidx = numel(ranki): -1 : 1
                    bbox = bboxes(ranki(bidx), :);
                    img_box = drawBox(img_box, bbox, 3, cmap(bidx, :));
                end
            end
            img_box_path = fullfile(imgs_path, sprintf('%s_%03d_i%02d_box.jpg', cls_name, iidx, itr));
            imwrite(img_box, img_box_path, 'jpg');
        end
        
        
        % final localization results ----------------------------------------------
        % load the last localization results ('saliency', 'conf_acc')
        load(fullfile(resultsFolder,'localization',cls_name, sprintf('sai%03d_i%02d.mat', iidx, num_max_iteration)) );
        
        % sorting raw confidences
        %[~, ranki] = sort(saliency, 'descend');
        [ ranki, saliv ] = select_kbestbox(bboxes', saliency, 1);
        
        % ground-truth bounding boxes
        img_res = img_org;
        for gidx = 1 : ngtboxes
            bbox = gt_boxes(gidx, :);
            img_res = drawBox(img_res, bbox, 6, [0, 0, 0]);
            img_res = drawBox(img_res, bbox, 3, [255, 255, 255]);
        end
        
        % estimated bounding box (top 1)
        bbox = bboxes(ranki(1), :);
        img_res = drawBox(img_res, bbox, 6, [100, 0, 0]);
        img_res = drawBox(img_res, bbox, 3, [255, 0, 0]);
        
        img_res_path = fullfile(imgs_path, sprintf('%s_%03d_res.jpg', cls_name, iidx));
        imwrite(img_res, img_res_path, 'jpg');
        
        % object box with part boxes
        img_res2 = repmat(rgb2gray(img_org), [1, 1, 3]);
        part_idx = crop_boxset(bboxes', ranki(1), conf_acc, 6);
        if numel(part_idx) > 1
            part_idx(1) = []; % eliminate the object box
            cmap = vals2colormap(conf_acc(part_idx), 'jet');
            cmap = ceil(cmap .* 255);
            for bidx = numel(part_idx): -1 : 1
                pbox = bboxes( part_idx(bidx), :);
                img_res2 = drawBox(img_res2, pbox, 6, cmap(bidx, :));
            end
        end
        img_res2 = drawBox(img_res2, bbox, 10, [100, 0, 0]);
        img_res2 = drawBox(img_res2, bbox, 6, [255, 0, 0]);
        img_res_path2 = fullfile(imgs_path, sprintf('%s_%03d_res2.jpg', cls_name, iidx));
        imwrite(img_res2, img_res_path2, 'jpg');

    end
end

