% Original author: Suha Kwak, Inria-Paris, WILLOW Project
% Modified by Michael Smith to be compatible with my new data format
function vis_CorLoc(name, resultsFolder, imageListFile, num_max_iteration)

% Load in image IDs
try
    load(imageListFile);
    classes = imagesByClass.keys;
    nclass = length(classes);
catch
    error('Unable to load data; please run localization.');
end

resultsDir_loc = fullfile(resultsFolder, 'localization');

nimage = sum(cellfun('length',imagesByClass.values));

% parameters
nMaxBBoxDisp = 5;				% max # displayed bounding boxes


% path to the images and webpages
webp_path = fullfile(resultsFolder, 'Webpage');
if ~exist(webp_path, 'dir')
    mkdir(webp_path);
end

imgs_path = fullfile(webp_path, 'img');
if ~exist(imgs_path, 'dir')
    mkdir(imgs_path);
end

% path to the file summarizing performance
perf_path = fullfile(resultsFolder, 'perf.mat');

currentImageNum = 1;

% =========================================================================
% CALCULATE CORLOC SCORES

% overlap ratio
overlap_list = zeros(nimage, num_max_iteration);

% success or not (0: fail, 1: success, 2: negative class)
success_list = zeros(nimage, num_max_iteration, 'uint8');

img_list_eva = cell(nclass, 1);

for cidx = 1 : length(classes)
    cls_name = classes{cidx};
    
    imageIdList = imagesByClass(cls_name);
    
    img_list_eva{cidx} = currentImageNum : currentImageNum + length(imagesByClass(cls_name)) - 1;
    
    for iidx = 1 : length(imagesByClass(cls_name))
        
        fprintf('Calculating score for image %i/%i for class %s\n', currentImageNum, nimage, cls_name);
        
        currentImageNum = currentImageNum + 1;
        
        imageId = imageIdList{iidx};
        
        % path to the co-localization results
        res_path = fullfile(resultsFolder, cls_name);
        
        
        % load image/boxes ('feat.img', 'feat.boxes')
        idata   = loadView_seg(imageId, res_path);
        
        % Bounding boxes
        bboxes  = frame2box(idata.frame)';
        % Original image
        img_org = imread(idata.fileName);
        
        % load ground-truth bounding boxes ("bbox_list")
        load(fullfile(resultsFolder, cls_name, [imageId, '.mat']));
        gt_boxes = cell2mat(bbox_list');
        ngtboxes = size(gt_boxes, 1);
        
        % ignore images without GT boxes (or negative images)
        if isempty(gt_boxes)
            overlap_list(currentImageNum - 1, :) = -1;
            success_list(currentImageNum - 1, :) = 2;
            continue;
        end
        
        % box to rect
        gt_rects = gt_boxes;
        gt_rects(:, 3:4) = gt_rects(:, 3:4) - gt_rects(:, 1:2) + 1;
        ngtrect = size(gt_rects, 1);
        
        
        for itr = 1 : num_max_iteration
            % load localization results ('saliency', 'conf_acc')
            load(fullfile(resultsFolder,'localization',cls_name, sprintf('sai%03d_i%02d.mat', iidx, itr)) );
            
            % best rectangle
            [saliv, ranki] = sort(saliency);
            best_rect = bboxes(ranki(end), :);
            best_rect(:, 3:4) = best_rect(:, 3:4) - best_rect(:, 1:2) + 1;
            
            % intersection/union areas
            int_area = rectint(best_rect, gt_rects);
            uni_area = zeros(1, ngtrect);
            for gidx = 1 : ngtrect
                uni_area(gidx) = prod(best_rect(3:4)) + prod(gt_rects(gidx, 3:4));
            end
            uni_area = uni_area - int_area;
            
            ovl_ratio = int_area ./ uni_area;
            overlap_list(currentImageNum - 1, itr) = max(ovl_ratio);
            if max(ovl_ratio) > 0.5
                success_list(currentImageNum - 1, itr) = 1;
            end
        end
    end
    
end



% summary per class
fprintf('Summarizing performance...\n');
oratio_all_cls  = cell(nclass, 1);
success_all_cls = cell(nclass, 1);

for cidx = 1 : nclass
    oratio_all_cls{cidx}  = overlap_list(img_list_eva{cidx}, :);
    success_all_cls{cidx} = success_list(img_list_eva{cidx}, :);
end


% summary for all
corLoc_cls = zeros(nclass, num_max_iteration);
oratio_cls = zeros(nclass, num_max_iteration);

for cidx = 1 : nclass
    oratio_list = oratio_all_cls{cidx};
    oratio_list = oratio_list(oratio_list(:, 1) >= 0, :);	% ignore negative images
    cls_nimage = size(oratio_list, 1);
    
    mean_oratio = mean(oratio_list, 1);
    oratio_cls(cidx, :) = mean_oratio;
    
    corLoc_val = sum(oratio_list > 0.5, 1) ./ cls_nimage;
    corLoc_cls(cidx, :) = corLoc_val;
end

corLoc_avg = mean(corLoc_cls, 1);
oratio_avg = mean(oratio_cls, 1);

save(perf_path, 'oratio_all_cls', 'success_all_cls', ...
    'corLoc_cls', 'corLoc_avg', ...
    'oratio_cls', 'oratio_avg');



% =========================================================================
% GRAPHS

fprintf('Draw figures\n');

% per class
for cidx = 1 : nclass
    oratio_list = oratio_all_cls{cidx};
    oratio_list = oratio_list(oratio_list(:, 1) >= 0, :);	% ignore negative images
    
    cls_name = classes{cidx};
    cls_nimage = size(oratio_list, 1);
    
    mean_ovrp_ratio = mean(oratio_list, 1);
    figure('Visible', 'off');
    plot(1:num_max_iteration, mean_ovrp_ratio, '-kd', 'LineWidth', 3, ...
        'Color', [.8, .1, .8], ...
        'MarkerSize', 7, ...
        'MarkerEdgeColor', [.8, .1, .8],...
        'MarkerFaceColor', [.8, .1, .8]);
    xlabel('Iterations');
    ylabel('Overlap ratio');
    axis([1, num_max_iteration, 0, 1]);
    set(gcf, 'Color', 'w');
    % set(gcf, 'Position', [500, 500, 320, 200]);
    fig_path = fullfile(imgs_path, sprintf('oratio_%s.png', cls_name));
    saveas(gcf, fig_path, 'png');
    close;
    
    corLoc_val = sum(oratio_list > 0.5, 1) ./ cls_nimage;
    figure('Visible', 'off');
    plot(1:num_max_iteration, corLoc_val, '-kd', 'LineWidth', 3, ...
        'Color', [.8, .1, .8], ...
        'MarkerSize', 7, ...
        'MarkerEdgeColor', [.8, .1, .8],...
        'MarkerFaceColor', [.8, .1, .8]);
    xlabel('Iterations');
    ylabel('CorLoc');
    axis([1, num_max_iteration, 0, 1]);
    set(gcf, 'Color', 'w');
    % set(gcf, 'Position', [500, 500, 320, 200]);
    fig_path = fullfile(imgs_path, sprintf('corloc_%s.png', cls_name));
    saveas(gcf, fig_path, 'png');
    close;
end

% overall
figure('Visible', 'off');
plot(1:num_max_iteration, oratio_avg, '-kd', 'LineWidth', 3, ...
    'Color', [.8, .1, .8], ...
    'MarkerSize', 7, ...
    'MarkerEdgeColor', [.8, .1, .8],...
    'MarkerFaceColor', [.8, .1, .8]);
xlabel('Iterations');
ylabel('Overlap ratio');
axis([1, num_max_iteration, 0, 1]);
set(gcf, 'Color', 'w');
% set(gcf, 'Position', [500, 500, 320, 200]);
fig_path = fullfile(imgs_path, 'oratio_all.png');
saveas(gcf, fig_path, 'png');
close;

figure('Visible', 'off');
plot(1:num_max_iteration, corLoc_avg, '-kd', 'LineWidth', 3, ...
    'Color', [.8, .1, .8], ...
    'MarkerSize', 7, ...
    'MarkerEdgeColor', [.8, .1, .8],...
    'MarkerFaceColor', [.8, .1, .8]);
xlabel('Iterations');
ylabel('CorLoc');
axis([1, num_max_iteration, 0, 1]);
set(gcf, 'Color', 'w');
% set(gcf, 'Position', [500, 500, 320, 200]);
fig_path = fullfile(imgs_path, 'corloc_all.png');
saveas(gcf, fig_path, 'png');
close;

