% By Michael Smith
function  library_setup(rp_root, vl_root )

if exist('RP_mex', 'file') == 0
    run(fullfile(rp_root,'setup.m'));
end
try
    temp = vl_version();
catch
    run(fullfile(vl_root, 'toolbox','vl_setup.m'));
end

