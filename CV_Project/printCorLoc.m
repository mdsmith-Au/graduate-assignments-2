% By Michael Smith
% Gets corloc score for given dataset and prints
% CorLoc score must have been precomputed by vis_CorLoc.m
% This is quick and dirty for the report and uses hardcoded stuff
function  printCorLoc( name )
classes = {'aeroplane', 'bicycle', 'boat', 'bus', 'horse', 'motorbike'};

%root_dataset_folder = '~/localDrive/msmith/';
root_dataset_folder = 'c:/users/michael smith/Desktop/stuff/';

folder_for_particular_dataset = [root_dataset_folder, name,'_results'];

load(fullfile(folder_for_particular_dataset,'perf.mat'));
figure;

plot(1:10, corLoc_cls,'LineWidth',2);
legend(classes);
ylabel('CorLoc score');
xlabel('Iterations');
ylim([0,1]);
fprintf('Airplane | Bike | Boat | Bus | Horse | Motorbike | Mean\n');
for i = 1 : 6
   fprintf([num2str(round(corLoc_cls(i,10),2)),' & ']);
end
fprintf(num2str(round(mean(corLoc_cls(1:6,10)),2)));
fprintf('\n');
end

