% By Michael Smith
function  paths = getAllPaths( rp_root, voc_devkit )

paths = [genpath(voc_devkit), genpath('./tools'), genpath(rp_root), genpath('./commonFunctions'), genpath('./hough-match'), genpath('./HOG')];

end

