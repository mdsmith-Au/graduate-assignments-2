% Author: Michael Smith
% Some parts (i.e. line 76-147) taken from the work of Suha Kwak, Inria-Paris, WILLOW Project
% This function introduces functionality capable of loading data from any
% PASCAL VOC dataset.  Previously, Suha Kwak used a predefined image list
% file for which the generation code was not available
% This function replaces several different functions used by the author.
function  [resultsFolder, imageListFile] = preprocess_dataset( classes, name, prune )
%PREPROCESS_DATASET Process 2007-2012 VOC dataset

% initialize VOC options
VOCinit;

resultsFolder = [VOCopts.datadir, name, '_results'];

imageListFile = fullfile(resultsFolder,'imageList.mat');

nclass  = length(classes);

gist_param.orientationsPerScale = [8 8 8 8];
gist_param.imageSize = [128 128];
gist_param.numberBlocks = 4;
gist_param.fc_prefilt = 4;
nfeat_gist = sum(gist_param.orientationsPerScale)*gist_param.numberBlocks^2;

params_rp = LoadConfigFile('rp_4segs.mat');

% Create results folder
if exist(resultsFolder, 'dir') == 0
    mkdir(resultsFolder);
end

% Get all images of given class

% load 'train' image set for class
imagesByClass = containers.Map;

for class = classes
    classstr = char(class);
    trainFile = fopen(sprintf(VOCopts.clsimgsetpath,classstr,'trainval'));

    [ids,classifier.gt]=textscan(trainFile,'%s %d');

    fclose(trainFile);

    imgNames = ids{1};
    imgClassPresence = ids{2};
    
    prunedIds = imgNames(imgClassPresence == 1);
    
    if (prune)
        numImages = length(prunedIds);

        for idx = 1 : length(prunedIds)
           id = prunedIds{idx};
           rec = PASreadrecord(sprintf(VOCopts.annopath, id));
           for obj = rec.objects
               if (obj.difficult == 1 || obj.truncated == 1)
                   prunedIds{idx} = [];
                   break;
               end
           end

        end

        imagesByClass(classstr) = prunedIds(~cellfun('isempty',prunedIds));
        fprintf('After pruning, %i/%i images remaining in class %s\n', length(imagesByClass(classstr)), numImages, classstr);
    else
        imagesByClass(classstr) = prunedIds;
        fprintf('%i images in class %s\n', length(prunedIds), classstr);
    end
end


totalNumImgs = sum(cellfun('length',imagesByClass.values));
parfor_progress(totalNumImgs);

imgpathTemp = VOCopts.imgpath;
annopathTemp = VOCopts.annopath;

for cidx = 1 : nclass
    class_name = classes{cidx};
    
    % make directory = class name
    class_folder = fullfile(resultsFolder, class_name);
    if exist(class_folder, 'dir') == 0
        mkdir(class_folder);
    end
    
    indices = imagesByClass(class_name);
    numImgsinClass = length(indices);
    
    fprintf('Processing class %s which contains %i images.\n', class_name, numImgsinClass);
    
    for iidx = 1 : numImgsinClass
        img_id = indices{iidx};
        img_path = sprintf(imgpathTemp, img_id);
        ann_path = sprintf(annopathTemp, img_id);
        mat_path = fullfile(class_folder, [img_id,'.mat']);
        new_img_path = fullfile(class_folder, [img_id, '.jpg']);
        
        if exist(mat_path, 'file') == 0 || exist(new_img_path,'file') == 0
            % annotation: classes and poses of objects
            anno = PASreadrecord(ann_path);
            obj_list = anno.objects;
            obj_class_list = {obj_list.class};
            obj_bbox_list  = {obj_list.bbox};	% [xmin, ymin, xmax, ymax]
            
            % find objects of the target class, and their bboxes
            obj_oidx  = cellfun(@(x) strcmpi(x, class_name), obj_class_list);
            bbox_list = obj_bbox_list(obj_oidx);
            
            % copy this image into the folder corresponding to the class
            copyfile(img_path, new_img_path);
            
            % compute GIST, save data to mat file
            img = imread(img_path);
            
            [gist_feat, ~] = LMgist(img, '', gist_param);
            
            % Gist features calculated, bounding boxes saved
            % We now need to extract potential bounding boxes and
            % associated descriptors
            
            % suha: images should have 3 channels
            % for running "extract_segfeat_hog" without problem.
            if ndims(img) < 3
                img = cat(3, img, img, img);
            elseif size(img, 3) == 1
                img = cat(3, img(:, :, 1), img(:, :, 1), img(:, :, 1));
            end
            img = standarizeImage(img);
            
            % compute segment proposals for the given image
            seg = RP(img, params_rp); %[xmin, ymin, xmax, ymax]
            seg = [seg; 1, 1, size(img,2), size(img,1)]; % add a whole box
            
            seg2 = struct('coords',seg);
            % Bounding boxes extracted -> find features
            feat = extract_segfeat_hog(img,seg2);
            
            savefile = matfile(mat_path, 'Writable', true);
            savefile.gist_feat = gist_feat;
            savefile.bbox_list = bbox_list;
            savefile.feat = feat;
            
        end
        parfor_progress;
    end
    
    
end

parfor_progress(0);

fprintf('Saving list of images by class...\n');
save(imageListFile, 'imagesByClass');

fprintf('Preprocessing done.\n');
end

