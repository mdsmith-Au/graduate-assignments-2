% By Michael Smith
rp_root = './rp-master';
vl_root = './vlfeat-0.9.20';
% 2007 devkit
voc_devkit = './VOCdevkit';
% 2012 devkit ----- Uncomment this line to use it (and comment the above
% line)
%voc_devkit = './VOCdevkit2012';

addpath(getAllPaths(rp_root, voc_devkit));

% Setup libraries
library_setup(rp_root, vl_root);

% Define dataset classes
classes = {'aeroplane', 'bicycle', 'boat', 'bus', 'horse', 'motorbike'};
%classes = {'aeroplane', 'bicycle'};
% classes={...
%         'aeroplane'
%         'bicycle'
%         'bird'
%         'boat'
%         'bottle'
%         'bus'
%         'car'
%         'cat'
%         'chair'
%         'cow'
%         'diningtable'
%         'dog'
%         'horse'
%         'motorbike'
%         'person'
%         'pottedplant'
%         'sheep'
%         'sofa'
%         'train'
%         'tvmonitor'}';

name = '2007_figure_gen_my_code_pruned';

% Prune setting
prune = 1;

[resultsFolder, imageListFile] = preprocess_dataset( classes, name, prune);

localization(name, resultsFolder, imageListFile,10,10);

vis_results(name, resultsFolder, imageListFile, 10);
vis_CorLoc(name, resultsFolder, imageListFile, 10);

rmpath(getAllPaths(rp_root, voc_devkit));
