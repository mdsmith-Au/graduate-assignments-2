
%%%%%%%%%%%%%%%%%%%%%%% file typeinst.tex %%%%%%%%%%%%%%%%%%%%%%%%%
%
% This is the LaTeX source for the instructions to authors using
% the LaTeX document class 'llncs.cls' for contributions to
% the Lecture Notes in Computer Sciences series.
% http://www.springer.com/lncs       Springer Heidelberg 2006/05/04
%
% It may be used as a template for your own input - copy it
% to a new file with a new name and use it as the basis
% for your article.
%
% NB: the document class 'llncs' has its own and detailed documentation, see
% ftp://ftp.springer.de/data/pubftp/pub/tex/latex/llncs/latex2e/llncsdoc.pdf
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\documentclass[runningheads,a4paper]{llncs}

\usepackage{amssymb}
\setcounter{tocdepth}{3}
\usepackage{graphicx}

\usepackage{url}
\urldef{\mailsa}\path|msmith@cim.mcgill.ca|
\newcommand{\keywords}[1]{\par\addvspace\baselineskip
\noindent\keywordname\enspace\ignorespaces#1}

\begin{document}

\mainmatter  % start of an individual contribution

% first the title is needed
\title{ECSE 626 Literature Review}

% a short form should be given in case it is too long for the running head
\titlerunning{ECSE 626 Literature Review}

% the name(s) of the author(s) follow(s) next
%
%
\author{Michael Smith}
%
\authorrunning{Michael Smith}
% (feature abused for this document to repeat the title also on left hand pages)

% the affiliations are given next; don't give your e-mail address
% unless you accept that it will be published
\institute{McGill University}
%
% NB: a more complex sample for affiliations and the mapping to the
% corresponding authors can be found in the file "llncs.dem"
% (search for the string "\mainmatter" where a contribution starts).
% "llncs.dem" accompanies the document class "llncs.cls".
%

\toctitle{ECSE 626 Literature Review}
\tocauthor{}
\maketitle

\begin{abstract}
This paper provides a brief summary of work performed in the field of object detection with respect to unsupervised learning.  Five papers spanning a decade are summarized and compared with a particular focus on statistical methods.
\end{abstract}

\section{Introduction}
As noted by Sivic \textit{et al.}\cite{1541280} and  Cho \textit{et al.}\cite{7298724} , object detection as a problem has typically involved some form of supervised learning.  Unfortunately, annotating data is both expensive and may introduce some unforeseen bias. Unsupervised learning, if successful, stands to bypass those issues entirely making it a compelling research topic.
\section{Review}
\subsection{Discovering objects and their location in images}
\paragraph{}
The very first paper to discuss the issue of unsupervised learning in conjunction with object detection was presented by Sivic \textit{et al.}\cite{1541280} who was motivated by promising unsupervised learning results achieved in other fields such as speech recognition or machine translation. Sivic \textit{et al.} approached the object detection problem as one where images are first categorized and the objects therein subsequently localized within the image.  SIFT descriptors, calculated on areas of interest (i.e. affine covariant regions\cite{Mikolajczyk:2002:AII:645315.649184}) are used to obtain visual words.  An alternate visual word configuration better suited to segmentation is also introduced; it makes note of areas where pairs of visual words occur together so as to allow for an entire object (rather than most of one) to be segmented.  The final step applies the probabilistic Latent Semantic Analysis (pLSA) model\footnote{A Latent Dirichlet Allocation (LDA) model was also found to give the same results.} to determine the category and location of objects in images.  Note that the number of categories is specified by the researchers.  The representation within the model is that of `bag of words', which by definition discards any spatial information.  pLSA itself is a statistical model, where the model parameters are found through maximum likelihood estimation and expectation maximization as seen in class.

\paragraph{}
Experimental results surrounding object classification are quite good, being comparable to (at the time) state of the art learning approaches in some respects and worse in others despite the lack of any supervision other than fixing the number of categories.  In particular, the authors experienced difficulties where their algorithm would identify backgrounds as important rather than the desired object in the foreground.  Experiments with segmentation were also run, with results proving to be a mixed at best, only identifying approximately 50\% of the object correctly.  Unfortunately, the authors do not focus on the segmentation problem and as a result they chose not to provide any comparisons to other work making thorough analysis difficult.  At the very least, however, it is clear that despite the use of techniques that discard spatial information localization within an image of a certain object is still possible to some degree.

\subsection{Using Multiple Segmentations to Discover Objects and their Extent in Image Collections}
\paragraph{}
The following year, two more papers were published in CVPR.  The first \cite{1640948} was written by Russell \textit{et al.} and expands upon the work of \cite{1541280} (the same authors were involved in both) by focusing on the spatial properties neglected in their previous work.  They treat the object recognition and segmentation problems as a grouping problem, and based on results by Hoiem \textit{et al.}\cite{1541316} they theorized that if an image were to be segmented multiple times in different ways similar segments (those that can be grouped together) can be considered an object category.  Along the same lines, segments that can be explained by combining multiple groups of segments consist of multiple objects.  In practice, the algorithm first computes the multiple segmentations using Normalized Cuts\cite{609407} and then computes the words for each segment in a `bag of words' model, in exactly the same manner as \cite{1541280}\footnote{This means that the requirement to manually specify the number of categories persists as well.}. The final step is to find the category each segment belongs to, which is once again done in the same manner as \cite{1541280} except the images are now segments and Latent Dirichlet Allocation is used instead of probabilistic Latent Semantic Analysis resulting in a slightly different likelihood function that needs to be maximized.  Overall, the proposed algorithm can be considered to be a `wrapper' over the one they previously proposed with the key difference being that it takes advantage of similarity between segments in addition to similarity between descriptors.
\paragraph{}
Like their previous work, they report quantitative results separately for classifying entire images and segmenting the objects.  With respect to classification, their results are approximately equal to their previous work \cite{1541280}; better in some categories and worse in others.  It is worth noting that unlike in their previous work, pLSA and LDA in fact perform quite differently as more categories are used which causes pLSA to overfit the data.  With regards to segmentation, however, compared to \cite{1541280} they consistently achieve better segmentation.  Considering the algorithm is based on segmentation in the first place, this is not surprising.  All that said, while the quantitative results appear good a qualitative analysis shows that even under good conditions the segmentations are not consistent even for objects of the same category.  For example, segmentations of faces would appear to have a 50\% chance of including the shoulders; similarly, segmentations of bikes contain a road perhaps 25\% of the time.  Clearly, more work is required before the algorithm can be used in a production environment.

\subsection{Unsupervised Learning of Categories from Sets of Partially Matching Image Features}
\paragraph{}
The second CVPR 2006 paper was written by Grauman \textit{et al.}\cite{1640737}.  The key contribution of their paper was similar to that of \cite{1640948} in that they formulate the object detection problem as a grouping problem.  The algorithm begins by calculating SIFT features for each image, although unlike \cite{1541280} the SIFT features are processed through PCA to reduce the dimensionality of the vectors as seen in class.  These features are then processed by a pyramid match kernel (introduced by the authors previously in \cite{1544890}) which computes pairwise affinities between images.  In theory, this represents how well matched different images are while avoiding sources of bias from different backgrounds or occlusion.  Spectral clustering is then applied to find the categories\footnote{Note that once again the authors manually specify the number of categories the algorithm should attempt to find.} (or dominant clusters), which are further refined into what the authors refer to as ``prototypical examples'', defined as a good representative of a certain category.  These ``prototypical examples'' are found by recomputing the affinities between images and choosing the examples which demonstrate the greatest consistency with respect to other category examples.  The final step predicts the category of new input images using the examples found earlier in conjunction with a Support Vector Machine.  An optional step exists which allows for a human to intervene in the learning process and add some annotations, transforming the algorithm into a semi-supervised approach if used.  Lastly, note that segmentation is not possible with this technique.
\paragraph{}
When it comes to results, the authors claim that their classifier is ``reasonably accurate'' in the unsupervised case yet they fail to provide a comparison against any other technique be it supervised or not.  In addition, the results are not presented in a format that allows for comparison to other work making any objective analysis of the algorithm extremely difficult barring an attempt to reproduce the results.  The only result worth noting is that when the semi-supervised version of their algorithm is used, performance improves as the amount of supervision increases.  In the end, the lack of comparisons against other algorithms combined with the algorithm's simplicity compared to others such as \cite{1640948} leads one to believe that the algorithm is quite simply not as capable as the authors claim.

\subsection{``Clustering by Composition'' - Unsupervised Discovery of Image Categories}
\paragraph{}
Following the publication of the above three papers, the only paper of note to discuss the issue of unsupervised learning with respect to object detection was published by Faktor \textit{et al.} \cite{6684535} in 2012.  Faktor \textit{et al.} were motivated by the observation that previous work such as \cite{1541280}, \cite{1640948} or \cite{1640737} all relied on some assumption of a `common model' that in practice does not hold true across all datasets, particularly more challenging ones.  Rather, the authors proposed using ``sophisticated images affinities'' based on work in \cite{NIPS2006_3127}.  The key idea is that part of one image can be composed using similar parts from other images\footnote{The authors note that the regions are usually quite different from those found via segmentation due to the different properties enforced on them; this is a key difference between this paper and \cite{1640737}.}. Regions with a low probability of occurring at random are prioritized, regardless of how often they may occur in the dataset; in fact, a desirable match may only occur once in a dataset.  Such affinities between images would be impossible to find using the techniques proposed in the previously discussed papers as there would be no commonly occurring shape or region to find.  The final step is clustering\footnote{Note that once again the researchers specify the number of categories.}, defined in this case as the ability for a given image to be reconstructed using only images within the cluster.  An efficient algorithm capable of proposing which images should be searched next speeds up the process.  Note that like the previous algorithm segmentation is not possible.

\paragraph{}
Experimental results with respect to classification proved to be extremely competitive when compared against existing algorithms\footnote{Note that the comparisons were made against slightly newer algorithms than those discussed above although the results remain relevant.} at the time, producing a ``30\% relative improvement over current state of the art'' according to the authors.  The algorithm also proved reasonably successful at classification on a more challenging dataset for which comparisons against other algorithms were not available, achieving an accuracy of less than 70\% that while reasonable leaves room for improvement.  The authors make a final claim regarding very small datasets, stating that their algorithm performed very well despite occlusions, clutter and the fact that small datasets traditionally cause issues for unsupervised algorithms.  Unfortunately, proof to back up said final claim is lacking.

\subsection{Unsupervised Object Discovery and Localization in the Wild: Part-based Matching with Bottom-up Region Proposals}
\paragraph{}
The final paper to be discussed is the most recent, authored by Cho \textit{et al.}\cite{7298724} and published in CVPR 2015.  The authors define two techniques: part-based region matching and foreground localization.  The former uses \cite{Manen:2013:POP:2586117.2587333} to find potential regions with objects (or object parts)\footnote{The fact that both entire objects and their parts are used is reminiscent of the work in \cite{1640948}.} where the quality of a match between regions is represented using a probability that can be calculated using Bayes' theorem and the Hough transform.  This also allows for images to be grouped into categories based on their scores.  The latter technique uses the results of the former in conjunction with the notion that foreground objects have a high contrast when compared to the background.  The algorithm itself classifies and determines the location of objects in the images by alternating between the part-based region matching and foreground localization techniques in a manner similar to that of gradient descent.  Of note is the fact that unlike every other technique presented, the authors do not specify the number of categories the algorithm is to attempt to find.

\paragraph{}
The authors define a new evaluation metric previously unseen in the literature and test their algorithm on two different datasets, one more challenging than the other.  They provide extensive comparisons to the state of the art algorithms that use some form of supervision and demonstrate that their algorithm either outperforms or at least approximately matches their performance in both localization and classification despite not requiring any supervision at all.  These results are impressive considering that clutter, occlusion and viewpoint changes are featured throughout the data, all of which are known to cause problems to most computer vision algorithms.

\section{Discussion}
\paragraph{}
Overall, we see a common trend amongst all algorithms: the first step involves detecting regions or points of interest and comparing these points across images.  The second step involves grouping the images in some fashion, typically taking advantage of the distance metrics used in the first step.  As a result, it is safe to say that all five papers implement variations of the same idea yet produce drastically different results, demonstrating the importance of choosing the correct representation or parameters for the problem at hand.

\paragraph{}
It is also worth noting that until \cite{7298724}, the unsupervised learning approach had not produced acceptable results when faced with the object detection problem.  In isolation, the results could be considered acceptable but when compared to supervised techniques they often fell short\cite{7298724}.  It is only now that unsupervised learning has delivered results comparable to supervised learning approaches when faced with the same challenging images.  Considering that once standard assumptions such as a known number of categories have also been eliminated, the only remaining challenges facing unsupervised learning are those of increased accuracy and the presence of multiple distinct objects in an image; with the exception of \cite{1640948}, most algorithms assume a given image has only one object in the foreground.  
\paragraph{}
Overall, the future of unsupervised techniques looks very promising indeed.
\bibliography{biblio}{}
\bibliographystyle{splncs03}

\end{document}
