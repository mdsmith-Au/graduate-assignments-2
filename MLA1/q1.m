clear all;
close all;

% Load data
X = dlmread('hw1x.txt');
y = dlmread('hw1y.txt');

% Add constant
% NOTE: Taken care of automatically later by MATLAB in
% ridge regression
% See MATLAB documentation at
% http://www.mathworks.com/help/stats/ridge.html for details

% Randomize order of data for splitting test and train
permutation = randperm(size(X,1));
X = X(permutation,:);
y = y(permutation,:);

% Say first 80% (of random order) are training, rest are testing
numTrain = 0.8*size(X,1);

% Do data split into testing/training
X_train = X(1:numTrain,:);
y_train = y(1:numTrain,:);

X_test = X(numTrain+1:end,:);
y_test = y(numTrain+1:end,:);

% Define possible lambda values
% Alpha is the scikit-learn terminology
% Note lamda = 0 is 0.01 here to aid in graph plotting on a log scale
alphas = [0.01,0.1,1,10,100,1000];

% Calculate linear regression
% Note last argument = 0 means a constant term is added automatically
b = ridge(y_train,X_train,alphas,0);

rmse_train = zeros(size(alphas));
rmse_test = zeros(size(alphas));

% Calculate RMSE for each lambda
for i = 1 : size(alphas,2)
    % Get coefficients
    coeff = b(:,i);
    
    y_pred = [ones(size(X_train,1),1) X_train] * coeff;
    
    rmse_train(i) = sqrt(mean((y_pred - y_train).^2));
    
    y_pred2 = [ones(size(X_test,1),1) X_test] * coeff;
    
    rmse_test(i) = sqrt(mean((y_pred2 - y_test).^2));
    
end

figure;
semilogx(alphas, rmse_train,alphas,rmse_test);
legend('Training','Testing');
xlabel('Lambda (log)');
ylabel('RMSE');
title('RMSE Part C');

figure;
semilogx(alphas, sqrt(sum(b.^2,1)));
xlabel('Lambda (log)');
ylabel('L2 norm');
title('L2 Norm Part C');

figure;
semilogx(alphas, b)
title('Weights Part C');
xlabel('Lambda (log)');
ylabel('Weight value');
legend('W1','W2','W3','W4');

% Part d - define sigmas
sigmas = [0.1,0.5,1,5,10];

% Each entry is 30 features for a given sigma
X_train_norm = cell(size(sigmas,2),1);
X_test_norm = cell(size(sigmas,2),1);

% Define space of possible gaussian means
us = linspace(-10,10,10);

% Part d: reformat data
for i = 1 : size(sigmas,2)
    
    sigma = sigmas(i);
    
    for u = us
        
        X_train_norm{i} = horzcat(X_train_norm{i}, normpdf(X_train, u,sigma));
        X_test_norm{i} = horzcat(X_test_norm{i}, normpdf(X_test,u,sigma));
    end
    
end
% Data now reformatted into cell indexed by sigma with each sigma having
% Nx30 entries, 3 per mean for 10 means

% Get minimum RMSE from previous part to add as a constant line for part e
rmse_train_partc = min(rmse_train) * ones(size(sigmas));
rmse_test_partc = min(rmse_test) * ones(size(sigmas));

rmse_train = zeros(size(sigmas));
rmse_test = zeros(size(sigmas));

% Part e: regression with no regularization (lamda = 0)
for i = 1 : size(sigmas,2)
    Xtrain = X_train_norm{i};
    Xtest = X_test_norm{i};
    
    b = ridge(y_train, Xtrain,0,0);
    
    y_pred = [ones(size(Xtrain,1),1) Xtrain] * b;
    
    rmse_train(i) = sqrt(mean((y_pred - y_train).^2));
    
    y_pred = [ones(size(Xtest,1),1) Xtest] * b;
    
    rmse_test(i) = sqrt(mean((y_pred - y_test).^2));
end

figure;
loglog(sigmas, rmse_train,sigmas,rmse_test, sigmas, rmse_train_partc, sigmas, rmse_test_partc);
legend('Train','Test','Prev. Train (Min)','Prev. Test (Min)');
xlabel('Sigma (log)');
ylabel('RMSE (log)');
title('RMSE Part E');

% Cobble together all basis functions
Xtrain = [];
Xtest = [];

for i = 1 : size(X_train_norm,1)
    Xtrain = horzcat(Xtrain,X_train_norm{i});
    Xtest = horzcat(Xtest,X_test_norm{i});
end

alphas = [0.01,0.1,1,10,100,1000,10000];

% Run regression w/ all basis functions (part f)
b = ridge(y_train,Xtrain,alphas,0);

rmse_train = zeros(size(alphas));
rmse_test = zeros(size(alphas));

for i = 1 : size(alphas,2)
    coeff = b(:,i);
    
    y_pred = [ones(size(Xtrain,1),1) Xtrain] * coeff;
    
    rmse_train(i) = sqrt(mean((y_pred - y_train).^2));
    
    y_pred2 = [ones(size(Xtest,1),1) Xtest] * coeff;
    
    rmse_test(i) = sqrt(mean((y_pred2 - y_test).^2));
end

figure;
loglog(alphas, rmse_train, alphas, rmse_test);
legend('Train','Test');
xlabel('Lamda (log)');
ylabel('RMSE (log)');
title('RMSE Part F');

figure;
semilogx(alphas, sqrt(sum(b.^2,1)));
xlabel('Lamda (log)');
ylabel('L2 Norm');
title('L2 Norm Part F');

l2_01 = sqrt(sum(b(2:31,:).^2,1));
l2_05 = sqrt(sum(b(32:32+29,:).^2,1));
l2_1 = sqrt(sum(b(61:61+29,:).^2,1));
l2_5 = sqrt(sum(b(90:90+29,:).^2,1));
l2_10 = sqrt(sum(b(119:119+29,:).^2,1));

figure;
loglog(alphas, l2_01, alphas, l2_05, alphas, l2_1, alphas, l2_5, alphas, l2_10);
xlabel('Lamda (log)');
ylabel('L2 Norm (log)');
title('L2 Norm by sigma Part F');
legend('Sigma = 0.1','Sigma = 0.5','Sigma = 1','Sigma = 5','Sigma = 10');


% Part J

u = 0;
cont = 1;
sgima = 1;
prevValue = 0;
m = size(y_train,2);

learning_rate = 0.5;
w = 0;
best_rmse_test = realmax;
best_rmse_tests = [];
best_ws = [];
best_us = [];
old_rmse_test = realmax;
old_w = 0;
old_u = 0;
j = 1;
while(cont)
    % Normal regression part - step 2
    
    % Compute X processed by guassian
    Xtrain = normpdf(X_train, u,sigma);
    Xtest = normpdf(X_test,u,sigma);
    
    % Run ridge regression
    b = ridge(y_train,Xtrain,alphas,0);
    
    rmse_train = zeros(size(alphas));
    rmse_test = zeros(size(alphas));
    for i = 1 : size(alphas,2)
        coeff = b(:,i);

        y_pred = [ones(size(Xtrain,1),1) Xtrain] * coeff;

        rmse_train(i) = sqrt(mean((y_pred - y_train).^2));

        y_pred2 = [ones(size(Xtest,1),1) Xtest] * coeff;

        rmse_test(i) = sqrt(mean((y_pred2 - y_test).^2));
    end
    
    old_rmse_test = best_rmse_test;
    [best_rmse_test, minI] = min(rmse_test);
    best_rmse_tests(j) = best_rmse_test;
   
    
    % This w is the optimal weight vector for a given u
    old_w = w;
    w = b(:,minI);
    best_ws(j,:) = w;
    
    best_us(j) = u;
    
    % Gradient descent part
    
    % Calculate difference
    difference = 0;
    for i = 1 : m
        x = [1 Xtrain(i,:)]';
        y = y_train(i);
        difference = difference + ( ( (w' / (sigma * sqrt(2*m))) * exp( (-(x-u).^2)/(2* (sigma).^2) ) - y ) * ( (w' / (sigma * sqrt(2*m))) * (exp( (-(x-u).^2)/(2* (sigma).^2) ) .* (1/(sigma.^2) * (x-u)) )));
    end
    difference = difference / m;
    
    % Note: difference is -ve here -> want to go away from higher error!
    new_u = (u + learning_rate * difference);
    
    j = j + 1;
    if ((new_u - u) < 0.001)
        % If not over exec. limit, run random restarts for u
        if (j < 1000)
            t1 = -100;
            t2 = 100;
            u = (t2-t1).*rand(1,1) + t1;
        else
            cont = 0;
            u = old_u;
            w = old_w;
        end
    else
        %fprintf('Old u: %f\n',u);
        %fprintf('New u: %f\n',new_u);
        old_u = u;
        u = new_u;
    end
end

% Show algorithm behaviour
figure;
plot(1:(j-1), best_rmse_tests);
title('RMSE as a function of iteration number, part j');

[minV,minI] = min(best_rmse_tests);
u = best_us(minI);
w = best_ws(minI,:);
disp('Best u:');
disp(u);
disp('Best w:');
disp(w');
disp('Final RMSE:');
disp(minV);