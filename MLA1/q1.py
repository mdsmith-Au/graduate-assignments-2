import numpy as np
from sklearn.cross_validation import train_test_split
from sklearn import linear_model
from sklearn.metrics import mean_squared_error

def main():
    X = np.loadtxt("hw1x.txt")
    y = np.loadtxt("hw1y.txt")

    # Add 1 to X
    onesCol = np.ones((X.shape[0], 1))
    X = np.hstack((X, onesCol))

    X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.8)

    # Set regularization parameters (alpha or lambda depending who you ask)
    alphas = [0,0.1,1,10,100,1000]
    for a in alphas:

        clf = linear_model.Ridge(alpha=a, fit_intercept=False)
        clf.fit(X_train,y_train)

        predicted = clf.predict(X_test)
        error = mean_squared_error(y_test, predicted)

        print("For alpha " + str(a))
        print("Testing Error: " + str(error))

        predicted = clf.predict(X_train)
        error = mean_squared_error(y_train, predicted)
        print("Training Error: " + str(error))


if __name__ == "__main__":
    main()
