function q1_2(meanImage, imgSize,v, trainingImages, trainingImages2)

    numEigenVectors = [1,2,5,10,20,50,100,1456];

    reconstructedImages = zeros([size(numEigenVectors,2),size(trainingImages)]);
    reconstructionError = zeros([size(numEigenVectors,2),1]);

    index = 1;
    for numEigen = numEigenVectors

        % Take only top x eigenvectors
        v_top = v(end-numEigen+1:end,:);

        reconstructedImages(index,:,:) = bsxfun(@plus, (trainingImages2 * v_top')*v_top, meanImage);

        index = index + 1;
    end

    for i = 1 : size(numEigenVectors,2)
        reconstructionError(i) = mean(sqrt(sum((trainingImages - reshape(reconstructedImages(i,:,:),size(trainingImages))).^2,2)));
    end

    figure;
    title('Reconstruction Error');
    plot(numEigenVectors, reconstructionError);
    xlabel('Number of Eigenfaces');
    ylabel('Error');

    % Show 3 images
    figure;
    title('Reconstruction');
    numEigenVectors2 = [1,3, 4, 5,6];
    % Chosen randomly
    images = [ 7, 1213,789];
    index = 1;
    for i = 1 : size(images,2)
        subplot(size(images,2),size(numEigenVectors2,2)+1,index);
        imshow(uint8(reshape(trainingImages(images(i),:),imgSize)));
        title('Orig');
        index = index + 1;
        for j = 1 : size(numEigenVectors2,2)
            subplot(size(images,2),size(numEigenVectors2,2)+1,index);
            imshow(uint8(reshape(reconstructedImages(numEigenVectors2(j),images(i),:),imgSize)));
            title(['Rec., e = ',num2str(numEigenVectors(numEigenVectors2(j)))]);
            index = index + 1;
        end
    end
end