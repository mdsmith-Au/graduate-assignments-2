clear all;
close all;
[ trainingImages,testingImages,imgSize ] = load_data( );

meanImage = mean(trainingImages);

% Subtract mean from all images
trainingImages2 = bsxfun(@minus, trainingImages, meanImage);

% Calculate covariance matrix
C = trainingImages2 * trainingImages2';

[v_prime,d] = eig(C);

% Eigenfaces
v = v_prime' * trainingImages2;

v = normr(v);

q1_1(meanImage, imgSize,v,d);

q1_2(meanImage, imgSize,v, trainingImages,trainingImages2);

q1_3(meanImage, imgSize,v, trainingImages, trainingImages2, testingImages);

q1_4(meanImage, v, trainingImages2, testingImages);

