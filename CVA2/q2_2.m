function q2_2( trainingImages,testingImages,imgSize,v, trainingImages2,meanImages )
%Q2_2 

mkdir('q2_2');

eigenvec = [1,2,5,10,20,30,50,90,100,-1];

confFig = figure('name','Confusion Matrix');
subplotIndex = 1;

% For all eigenvectors
for i = 1 : size(eigenvec,2)
    
    keyList = {0,-15,15,-50,50,-90,90};
    valueList = {[],[],[],[],[],[],[]};

    representationTraining = containers.Map(keyList,valueList);
    representationTesting = containers.Map(keyList,valueList);

    actualTestLabels = containers.Map(keyList,valueList);

    allTestingImages = cell2mat(testingImages.values');
    
    for pose = trainingImages.keys
        poseNo = pose{1};
        
        if (eigenvec(i) == -1)
            numEigen = size(v(poseNo),1);
        else
            numEigen = eigenvec(i);
        end
        
        v_tmp = v(poseNo);
        v_top = v_tmp(end-numEigen+1:end,:);

        representationTraining(poseNo) = trainingImages2(poseNo) * v_top';
        representationTesting(poseNo) = bsxfun(@minus,allTestingImages,meanImages(poseNo)) * v_top';

        actualTestLabels(poseNo) = poseNo * ones([1, size(testingImages(poseNo),1)]);
    end
    
    actualTestLabels = cell2mat(actualTestLabels.values);
    
    distances = realmax * ones([1, size(allTestingImages,1)]);
    indices = zeros([1, size(allTestingImages,1)]);
    poses = zeros([1, size(allTestingImages,1)]);
    for pose = trainingImages.keys
        poseNo = pose{1};
        
        % Need to look at distances between train & test for this given
        % pose
        [D,I] = pdist2(representationTraining(poseNo), representationTesting(poseNo), 'euclidean','Smallest',1);
        better = D < distances;
        distances(better) = D(better);
        indices(better) = I(better);
        poses(better) = poseNo;
    end
    
    
    if (numEigen > 100)
        numEigenStr = 'all';
    else
        numEigenStr = num2str(numEigen);
    end
    
    recogRate = (nnz(poses == actualTestLabels)/size(actualTestLabels,2))*100;
    display(['Recognition rate for ',numEigenStr,' eigenvec. = ', num2str(recogRate),'%']);
    
    conf = confusionmat(actualTestLabels, poses);
    figure(confFig);
    subplot(2,5,subplotIndex);
    subplotIndex = subplotIndex + 1;
    imagesc(conf);
    title(['Eig = ', numEigenStr]);
    ylabel('Truth');
    xlabel('Hypothesis');
    
    
    % Display nearest training images
    if (numEigen == 50)
        correctFig = figure;
        badFig = figure;


        correctness = poses == actualTestLabels;
        imageNumC = 1;
        imageNumB = 1;
        goodImagesUsed = [];
        badImagesUsed = [];
        for j = 1 : numel(correctness)
            if (correctness(j) == 1 && imageNumC < 6)
                figure(correctFig);
                % Show original image
                subplot(3,2,imageNumC);
                origImage = trainingImages(poses(j));
                origImage = origImage(indices(j),:);
                imshow(uint8(reshape(origImage,imgSize)));
                title('Orig - Good');
                imageNumC = imageNumC + 1;
                subplot(3,2,imageNumC);
                testImage = allTestingImages(j,:);
                imshow(uint8(reshape(testImage,imgSize)));
                title('Test - Good');
                imageNumC = imageNumC + 1;
                goodImagesUsed = [goodImagesUsed, j];
                display(['Good image ',num2str(j),' → sub. label ', num2str(poses(j))]);
            elseif (correctness(j) == 0 && imageNumB < 6)
                figure(badFig);
                subplot(3,2,imageNumB);
                origImage = trainingImages(poses(j));
                origImage = origImage(indices(j),:);
                imshow(uint8(reshape(origImage,imgSize)));
                title('Orig - Bad');
                imageNumB = imageNumB + 1;
                subplot(3,2,imageNumB);
                testImage = allTestingImages(j,:);
                imshow(uint8(reshape(testImage,imgSize)));
                title('Test - Bad');
                imageNumB = imageNumB + 1;
                badImagesUsed = [badImagesUsed, j];
                display(['Bad image ',num2str(j),' → sub. label ', num2str(poses(j))]);
            elseif (imageNumB >=6 && imageNumC >=6 )
                print(correctFig,'q2_2/correct.png','-dpng','-r600');
                print(badFig,'q2_2/bad.png','-dpng','-r600');
                break;
            end
        end
    end
        
end
% End eigen vector for loop
save('imagesUsed2.mat','badImagesUsed','goodImagesUsed');

print(confFig,'q2_2/confusion.png','-dpng','-r600');


end

