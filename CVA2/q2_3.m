function q2_3( trainingImages,testingImages,imgSize,v, trainingImages2,meanImages  )
%Q2_3

load('imagesUsed2.mat');

numEigen = 50;
keyList = {0,-15,15,-50,50,-90,90};
valueList = {[],[],[],[],[],[],[]};

representationTraining = containers.Map(keyList,valueList);
representationTesting = containers.Map(keyList,valueList);
actualTestLabels = containers.Map(keyList,valueList);

allTestingImages = cell2mat(testingImages.values');

likelihoods_u = containers.Map(keyList,valueList);
likelihoods_covar = containers.Map(keyList,valueList);

totalNumTraining = size(cell2mat(trainingImages.values'),1);

% Calculate likelihoods for all poses
for pose = trainingImages.keys
    poseNo = pose{1};
    
    v_tmp = v(poseNo);
    v_top = v_tmp(end-numEigen+1:end,:);
    
    representationTraining(poseNo) = trainingImages2(poseNo) * v_top';
    representationTesting(poseNo) = bsxfun(@minus,allTestingImages,meanImages(poseNo)) * v_top';
    actualTestLabels(poseNo) = poseNo * ones([1, size(testingImages(poseNo),1)]);
    
    likelihoods_u(poseNo) = mean(representationTraining(poseNo));
    covariance = cov(representationTraining(poseNo));
    % Below is to deal with numerical instability
    likelihoods_covar(poseNo)  = covariance.*eye(size(covariance)) + 0.9*covariance.*~eye(size(covariance));
end

actualTestLabels = cell2mat(actualTestLabels.values);

likelihoods = zeros([ size(keyList,2), size(allTestingImages,1)]);
maxLikelihoods = zeros([ size(allTestingImages,1), 1]);

poses = zeros([1, size(allTestingImages,1)]);
idx = 1;
for pose = trainingImages.keys
    poseNo = pose{1};
    
    % Calculate probabilities
    prob = mvnpdf(representationTesting(poseNo), likelihoods_u(poseNo), likelihoods_covar(poseNo)) * (size(trainingImages(poseNo),1)/totalNumTraining);
    
    likelihoods(idx, :) = prob';
    
    better = prob > maxLikelihoods;
    maxLikelihoods(better) = prob(better);
    
    poses(better) = poseNo;
    
    idx = idx + 1;
    

end


recogRate = (nnz(poses == actualTestLabels)/size(actualTestLabels,2))*100;
display(['Recognition rate for prob = ', num2str(recogRate),'%']);


conf = confusionmat(actualTestLabels, poses);
figure('name','Confusion matrix prob.');
imagesc(conf);
title('Confusion Matrix (Probability)');
ylabel('Truth');
xlabel('Hypothesis');

% Display distrib. for same images as prev. question
goodFig = figure('name','Posterior: Good Images');
badFig = figure('name','Posterior: Bad Images');
index = 1;
xaxis = sort(cell2mat(keyList));

for img = goodImagesUsed
    figure(goodFig);
    subplot(3,1,index);
    plot(xaxis,likelihoods(:,img));
    title(['Posterior, good img # ',num2str(img),', sub. label ', num2str(poses(img))]);
    xlabel('Subject Labels');
    ylabel('Probability');
    index = index + 1;
end


index = 1;
for img = badImagesUsed
    figure(badFig);
    subplot(3,1,index);
    plot(xaxis,likelihoods(:,img));
    title(['Posterior, bad img # ',num2str(img),', sub. label ', num2str(poses(img))]);
    xlabel('Subject Labels');
    ylabel('Probability');
    index = index + 1;
end

end

