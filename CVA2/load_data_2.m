function [ trainingImages,testingImages,imgSize ] = load_data_2( )
%LOAD_DATA Summary of this function goes here
%   Detailed explanation goes here

% Location
loc = '~/localDrive/msmith/Color FERET Database';
%loc = 'C:\Users\Michael Smith\Desktop\Color FERET Database';

directory = dir(loc);

directory(1:2) = [];

numSubjects = size(directory,1);

% Get dimensions of one
imgSize = size(read(imageSet([loc,'/',directory(1).name]),1));
imgSize = imgSize(1:2)/2;

keyList = {0,-15,15,-50,50,-90,90};
valueList = {[],[],[],[],[],[],[]};

allImages = containers.Map(keyList,{{},{},{},{},{},{},{}});
trainingImages = containers.Map(keyList,valueList);
testingImages = containers.Map(keyList,valueList);

% Mapping
% 0->0
% 10-> 15
% 15->15
% 22.5 ->15
%25->15
% 40->50
% 45->50
% 60->50
%67.5->50
% 90->90


for d = 1 : numSubjects
    folder = [loc,'/',directory(d).name];
    set = imageSet(folder);
    
    % Iterate over all images and add to appropriate map
    for image = set.ImageLocation
        imageStr = image{1};
        [~,name,~] = fileparts(imageStr);
        code = strsplit(name,'_');
        code = lower(code{3});
        switch code
            case {'fa','fb','ba','bj','bk'}
                pose = 0;
            case {'rb','be','qr','bd'}
                pose = 15;
            case {'rc','bf','ql','bg'}
                pose = -15;
            case {'bc','ra','bb','hr'}
                pose = 50;
            case {'bh','rd','bi','hl'}
                pose = -50;
            case {'pl'}
                pose = -90;
            case {'pr','re'}
                pose = 90;
            otherwise
                display(['Error: pose not recognized for file ',imageStr]);
                pose = 0;
        end
        
        %data = double(reshape(rgb2gray(imresize(imread(imageStr),0.5)),[1, prod(imgSize)]));
        allImages(pose) = [allImages(pose); imageStr];
    end
end

% Load actual data
for pose = allImages.keys
    % Get num images
    poseNo = pose{1};
    imageList = allImages(poseNo);
    numImg = size(imageList,1);
    [trainInd,~,testInd] = dividerand(numImg,0.9,0,0.1);
    trainingImagesTemp = zeros(size(trainInd,2),prod(imgSize));
    testingImagesTemp = zeros(size(testInd,2),prod(imgSize));
    
    for i = 1 : size(trainInd,2)
        trainingImagesTemp(i,:) = double(reshape(imresize(rgb2gray(imread(imageList{trainInd(i)})),0.5),[1, prod(imgSize)]));
    end
    
    for i = 1 : size(testInd,2)
        testingImagesTemp(i,:) = double(reshape(imresize(rgb2gray(imread(imageList{testInd(i)})),0.5),[1, prod(imgSize)]));
    end
    
    trainingImages(poseNo) = trainingImagesTemp;
    testingImages(poseNo) = testingImagesTemp;
end

end

