function q1_4(meanImage, v, trainingImages2, testingImages)

    numEigen = 50;

    v_top = v(end-numEigen+1:end,:);

    representationTraining = trainingImages2 * v_top';
    representationTesting = bsxfun(@minus,testingImages,meanImage) * v_top';

    numSubjects = 52;

    likelihoods_u = zeros([numSubjects numEigen]);
    likelihoods_covar = zeros([numEigen, numEigen, numSubjects]);
    % Calculate likelihoods
    for sub = 1:numSubjects
        index1 = 28*(sub-1)+1;
        index2 = 28*(sub-1)+1+27;

        train = representationTraining(index1:index2,:);

        likelihoods_u(sub, :) = mean(train);
        covariance = cov(train);
        % Below is to deal with numerical instability
        covariance = covariance.*eye(size(covariance)) + 0.9*covariance.*~eye(size(covariance));
        likelihoods_covar(:,:,sub) = covariance;
    end


    load('imagesUsed');

    %Calculate posterior distribution

    % for all images
    %   for all labels
    %       calculate likelihood
    %   pick max likelihood -> assign label
    assignedLabels = zeros([1, size(representationTesting,1)]);
    posteriorDistrib = zeros([size(representationTesting,1), numSubjects]);
    for img = 1 : size(representationTesting,1)
        rep = repmat(representationTesting(img,:),[numSubjects,1]);
        prob = mvnpdf(rep, likelihoods_u, likelihoods_covar);
        posteriorDistrib(img,:) = prob;
        [~,I] = max(prob);
        assignedLabels(img) = I;
    end


    truelabels = int32(ceil((1:size(testingImages,1))/4));
    recogRate = nnz(truelabels == assignedLabels)/numel(assignedLabels);
    display(['Recognition rate of prob. classifier: ',num2str(recogRate*100),'%']);
    
    
    % Below is only to display distribution for same images as prev. quest
    goodFig = figure;
    badFig = figure;
    index = 1;
    for img = goodImagesUsed
        figure(goodFig);
        subplot(3,1,index);
        plot(posteriorDistrib(img,:));
        title(['Posterior, good img # ',num2str(img),', sub. label ', num2str(assignedLabels(img))]);
        xlabel('Subject Labels');
        ylabel('Probability');
        index = index + 1;
    end
    
    
    index = 1;
    for img = badImagesUsed
        figure(badFig);
        subplot(3,1,index);
        plot(posteriorDistrib(img,:));
        title(['Posterior, bad img # ',num2str(img),', sub. label ', num2str(assignedLabels(img))]);
        xlabel('Subject Labels');
        ylabel('Probability');
        index = index + 1;
    end
end