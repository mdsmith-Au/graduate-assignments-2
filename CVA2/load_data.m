function [ trainingImages,testingImages,imgSize ] = load_data( )
%LOAD_DATA Summary of this function goes here
%   Detailed explanation goes here

% Location
loc = '~/localDrive/msmith/Color FERET Database';
%loc = 'C:\Users\Michael Smith\Desktop\Color FERET Database';

directory = dir(loc);

directory(1:2) = [];

numSubjects = size(directory,1);

% Get dimensions of one 
imgSize = size(read(imageSet([loc,'/',directory(1).name]),1));
imgSize = imgSize(1:2)/2;

trainingImages = zeros(numSubjects*28, prod(imgSize));
testingImages = zeros(numSubjects*4, prod(imgSize));

indexTrain = 1;
indexTest = 1;

for d = 1 : numSubjects
   folder = [loc,'/',directory(d).name];
   set = imageSet(folder);
   [trainInd,~,testInd] = dividerand(set.Count,28/32,0,4/32);
   trainInd = datasample(trainInd,28,'Replace',false);
   testInd = datasample(testInd,4,'Replace',false);
   
   for i = trainInd
        trainingImages(indexTrain,:) = double(reshape(imresize(rgb2gray(read(set,i)),0.5),[1, prod(imgSize)]));
        indexTrain = indexTrain + 1;
   end
   
   for i = testInd
        testingImages(indexTest,:) = double(reshape(imresize(rgb2gray(read(set,i)),0.5),[1, prod(imgSize)]));
        indexTest = indexTest + 1;
   end
end


end

