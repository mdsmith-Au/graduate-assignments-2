[ trainingImages,testingImages,imgSize ] = load_data_2( );

keyList = {0,-15,15,-50,50,-90,90};
valueList = {[],[],[],[],[],[],[]};


meanImages = containers.Map(keyList,valueList);
v = containers.Map(keyList,valueList);
trainingImages2 = containers.Map(keyList,valueList);
for pose = keyList
    poseNo = pose{1};
    trainImgs = trainingImages(poseNo);
    meanImages(poseNo) = mean(trainImgs);
    trainingImages2(poseNo) = bsxfun(@minus, trainingImages(poseNo), meanImages(poseNo));
    
    C = trainingImages2(poseNo) * trainingImages2(poseNo)';
    
    [v_prime,~] = eig(C);
    
    v_tmp = v_prime' * trainingImages2(poseNo);
    v_tmp = normr(v_tmp);
    v(poseNo) = v_tmp;
end

q2_1( meanImages, v, imgSize);

q2_2( trainingImages,testingImages,imgSize,v, trainingImages2,meanImages);

q2_3( trainingImages,testingImages,imgSize,v, trainingImages2,meanImages  );