function  q2_1( meanImages, v_all, imgSize )

meanFig = figure('name','Mean Images');
index = 1;
for pose = meanImages.keys
    poseNo = pose{1};
    figure(meanFig);
    subplot(2,4,index);
    index = index + 1;
    
    imshow(uint8(reshape(meanImages(poseNo),imgSize)));
    title(['P = ',num2str(poseNo)]);

    v = v_all(poseNo);
    figure('name',['Principal Components - Pose = ', num2str(poseNo)]);
    for i = 1 : 10
        subplot(2,5,i);
        imagesc(reshape(v(size(v,1)-i+1,:), imgSize));
        title(['C = ', num2str(i)]);
    	set(gca,'XTickLabel','')
        set(gca,'YTickLabel','')
    end
    colormap gray;
end

end

