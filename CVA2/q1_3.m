function q1_3(meanImage, imgSize,v, trainingImages, trainingImages2, testingImages)

    eigenVectors = [1,2,5,10,20,50,100,1456];

    recogRate = zeros([1, size(eigenVectors,2)]);
    index = 1;

    for numEigen = eigenVectors
        % Take only top x eigenvectors
        v_top = v(end-numEigen+1:end,:);

        % Project to eigenspace
        representationTraining = trainingImages2 * v_top';
        representationTesting = bsxfun(@minus,testingImages,meanImage) * v_top';

        % Find Nearest-neighbour matches
        [~,I] = pdist2(representationTraining, representationTesting, 'euclidean', 'Smallest',1);

        % Label calculation
        truelabels = int32(ceil((1:size(testingImages,1))/4));
        predictedLabels = int32(ceil((I)/28));

        recogRate(index) = nnz(truelabels == predictedLabels)/numel(predictedLabels);
        index = index + 1;

        % For 50 eigenvectors show the images
        if (numEigen == 50)
            correctFig = figure;
            badFig = figure;

            correctness = truelabels == predictedLabels;
            imageNumC = 1;
            imageNumB = 1;
            goodImagesUsed = [];
            badImagesUsed = [];
            for i = 1 : numel(correctness)
                if (correctness(i) == 1 && imageNumC < 6)
                    figure(correctFig);
                    % Show original image
                    subplot(3,2,imageNumC);
                    imshow(uint8(reshape(trainingImages(I(i),:),imgSize)));
                    title('Orig - Good');
                    imageNumC = imageNumC + 1;
                    subplot(3,2,imageNumC);
                    imshow(uint8(reshape(testingImages(i,:),imgSize)));
                    title('Test - Good');
                    imageNumC = imageNumC + 1;
                    goodImagesUsed = [goodImagesUsed, i];
                    display(['Good image ',num2str(i),' → sub. label ', num2str(predictedLabels(i))]);
                elseif (correctness(i) == 0 && imageNumB < 6)
                    figure(badFig);
                    subplot(3,2,imageNumB);
                    imshow(uint8(reshape(trainingImages(I(i),:),imgSize)));
                    title('Orig - Bad');
                    imageNumB = imageNumB + 1;
                    subplot(3,2,imageNumB);
                    imshow(uint8(reshape(testingImages(i,:),imgSize)));
                    title('Test - Bad');
                    imageNumB = imageNumB + 1;
                    badImagesUsed = [badImagesUsed, i];
                    display(['Bad image ',num2str(i),' → sub. label ', num2str(predictedLabels(i))]);
                elseif (imageNumB >=6 && imageNumC >=6 )
                    break;
                end
            end
        end
    end

    figure;
    plot(eigenVectors, recogRate);
    title('Recognition Rate');
    xlabel('Eigenvectors');
    ylabel('Recognition rate');
    
    display(['Recog rate for ', num2str(eigenVectors(6)), ' eigenvec. = ', num2str(recogRate(6))]);
        
    txt1 = '\leftarrow 50 eigenvectors';
    
    text(50,recogRate(6),txt1);
    
    % Save which images we used for next part
    save('imagesUsed.mat','badImagesUsed','goodImagesUsed');
end