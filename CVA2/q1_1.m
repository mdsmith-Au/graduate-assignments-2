
function q1_1(meanImage, imgSize,v,d)
    figure;
    % Show mean image
    imshow(uint8(reshape(meanImage,imgSize)));
    title('Mean Image');

    figure;
    % Show principal components
    title('Principal Components');
    for i = 1 : 10
        subplot(2,5,i);
        imagesc(reshape(v(size(v,1)-i+1,:), imgSize));
        title(['C ',num2str(i)]);
        set(gca,'XTickLabel','')
        set(gca,'YTickLabel','')
    end
    colormap gray;

    % Calculate % of variance captured
    diagFlipped = flipud(diag(d));
    t1 = cumsum(diagFlipped)/sum(diagFlipped);

    figure;
    plot(t1);
    title('Variance captured');
    ylabel('Percentage of variance captured');
    xlabel('Number of principal components');
    
    txt1 = '\leftarrow 90% = 98 components';
    txt2 = '\leftarrow 95% = 268 components';
    text(98,t1(98),txt1);
    text(268,t1(268),txt2);

end