% For this part we make use of Mark Schmidt's library
% available at https://www.cs.ubc.ca/~schmidtm/Software/minFunc.html
clear all;
close all;

% Load data
X = dlmread('hw2x.dat');
y = dlmread('hw2y.dat');

% Library expects either 1 or 0 for classification - so we modify 0 = -1
y(y == 0) = -1;

% Regularization constant
lambda = 0.1;
options.Display = 0;

% Get indices for train/test - kfold
Indices = crossvalind('Kfold', size(X,1), 3);

plainErrorTrain = zeros(3,1);
polyErrorTrain = zeros(3,3);

plainErrorTest = zeros(3,1);
polyErrorTest = zeros(3,3);

% For k-fold - testing
for k = 1 : 3
   testX = X(Indices == k,:);
   trainX = X(Indices ~= k,:);
   
   testy = y(Indices == k);
   trainy = y(Indices ~= k);
   
   % Plain logistic reg (linear)
   % Note we are using L2 regularization
    regularLogistic = @(w)LogisticLoss(w,trainX,trainy);
    %fprintf('Training linear logistic regression model...\n');
    wLinear = minFunc(@penalizedL2,zeros(size(trainX,2),1),options,regularLogistic,lambda);

    trainingError = sum(trainy ~= sign(trainX*wLinear))/length(trainy);
    plainErrorTrain(k) = trainingError;
    display(['Training Error for plain k = ',num2str(k),' is ', num2str(trainingError)]);
    
    testingError = sum(testy ~= sign(testX*wLinear))/length(testy);
    plainErrorTest(k) = testingError;
    display(['Testing Error for plain k = ',num2str(k),' is ', num2str(testingError)]);
    
   % Polynomial logistic regression
   % Again using L2 regularization
    for d = 1 : 3
        Kernel = kernelPoly(trainX,trainX,d);
        polyLoss = @(u)LogisticLoss(u,Kernel,trainy);
        %fprintf('Training polynomial logistic regression model...\n');
        uPoly = minFunc(@penalizedKernelL2,zeros(size(trainX,1),1),options,Kernel,polyLoss,lambda);
        
        traingErrorPoly = sum(trainy ~= sign(Kernel*uPoly))/length(trainy);
        polyErrorTrain(k,d) = traingErrorPoly;
        display(['Training Error for poly. k = ',num2str(k),' with poly order ', num2str(d),' is ', num2str(traingErrorPoly)]);
        
        KernelTest = kernelPoly(testX,testX,d);
        [~,I] = pdist2(trainX, testX,'euclidean','Smallest',1);
        uPolyTest = uPoly(I);

        testingErrorPoly = sum(testy ~= sign(KernelTest*uPolyTest))/length(testy);
        polyErrorTest(k,d) = testingErrorPoly;
        display(['Testing Error for poly. k = ',num2str(k),' with poly order ', num2str(d),' is ', num2str(testingErrorPoly)]);
     
    end

end

% Graphs
figure;
plot(1:3, plainErrorTrain, 1:3, plainErrorTest, 1:3, polyErrorTrain(:,1), 1:3, polyErrorTrain(:,2), 1:3, polyErrorTrain(:,3), 1:3, polyErrorTest(:,1), 1:3, polyErrorTest(:,2), 1:3, polyErrorTest(:,3));
xlabel('K');
ylabel('Error');
legend('Plain Train', 'Plain Test', 'Poly Train d=1', 'Poly Train d=2', 'Poly Train d=3', 'Poly Test d=1', 'Poly Test d=2', 'Poly Test d=3');

