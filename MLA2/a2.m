% For this part we make use of Mark Schmidt's library
% available at https://www.cs.ubc.ca/~schmidtm/Software/minFunc.html
% The old original function is commented below

clear all;
close all;

% Load data
X = dlmread('hw2x.dat');
y = dlmread('hw2y.dat');

% Library expects either 1 or 0 for classification - so we modify 0 = -1
y(y == 0) = -1;

lambda = 0.01;

options.Display = 0;

d = 2;
K = kernelPoly(X,X,d);
lossFunction = @(u)LogisticLoss(u,K,y);
uPoly = minFunc(@penalizedKernelL2,zeros(size(X,1),1),options,K,lossFunction,lambda);

display('Parameters: ');
X'*uPoly

display('Training error:');
trainErr_poly = sum(y ~= sign(K*uPoly))/length(y)


% d= 1;
% K = kernelPoly(X,X,d);
% a = randi(2,size(y)) - 1;
% old_error = 0;
% cont = 1;
% learning_rate = 1;
% lamda = 0;
% 
% 
% numIter = 0;
% while(cont)
%     numIter = numIter + 1;
%     deltaerror = -(K.'*(y./(1+exp(y.*(K*a)))));
%     
%     new_a = a - learning_rate * deltaerror;
%     display(['delta J: ', num2str(mean(deltaerror))]);
%     if (sum(abs(a - new_a)) < 0.01)
%         break;
%     end
%     a = new_a;
%     old_error = deltaerror;
%     
% end
% display(['Ran for ', num2str(numIter), ' iterations.']);