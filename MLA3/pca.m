clear all;
close all;

data = dlmread('hw3pca.txt');

[trainInd,~,testInd] = dividerand(size(data,1),0.8,0,0.2);
train = data(trainInd,:);
test = data(testInd,:);

meandata = mean(train);

% Subtract mean from all data
train2 = bsxfun(@minus, train, meandata);

% Calculate covariance matrix
C = train2 * train2';

[v_prime,d] = eig(C);

% eigenvec
v = v_prime' * train2;

v = normr(v);

% Calculate % of variance captured
diagFlipped = flipud(diag(d));
t1 = cumsum(diagFlipped)/sum(diagFlipped);

figure;
plot(t1*100);
title('Variance captured');
ylabel('Percentage of variance captured');
xlabel('Number of principal components');

numEigenVectors = 1:size(v,1);

reconstructedData = zeros([size(numEigenVectors,2),size(train)]);
reconstructionError = zeros([size(numEigenVectors,2),1]);

index = 1;
for numEigen = numEigenVectors

    % Take only top x eigenvectors
    v_top = v(end-numEigen+1:end,:);

    reconstructedData(index,:,:) = bsxfun(@plus, (train2 * v_top')*v_top, meandata);

    index = index + 1;
end

for i = 1 : size(numEigenVectors,2)
    reconstructionError(i) = mean(sqrt(sum((train - reshape(reconstructedData(i,:,:),size(train))).^2,2)));
end

figure;
plot(numEigenVectors, reconstructionError);
xlabel('Number of Eigenvectors');
ylabel('Error');
title('Reconstruction Error - Training');

reconstructedData = zeros([size(numEigenVectors,2),size(test)]);
reconstructionError = zeros([size(numEigenVectors,2),1]);

index = 1;

meandatatest = mean(test);
test2 = bsxfun(@minus, test, meandatatest);

for numEigen = numEigenVectors

    % Take only top x eigenvectors
    v_top = v(end-numEigen+1:end,:);

    reconstructedData(index,:,:) = bsxfun(@plus, (test2 * v_top')*v_top, meandatatest);

    index = index + 1;
end

for i = 1 : size(numEigenVectors,2)
    reconstructionError(i) = mean(sqrt(sum((test - reshape(reconstructedData(i,:,:),size(test))).^2,2)));
end

figure;
plot(numEigenVectors, reconstructionError);
xlabel('Number of Eigenvectors');
ylabel('Error');
title('Reconstruction Error - Testing');
