function A1_Q2b ()
% Q2.2

% Read data
horse = imread('horse.jpg');
horse_g = rgb2gray(horse);
% Define feature spaces
intensities = reshape(horse_g, [size(horse_g,1)*size(horse_g,2), 1]);
rgb = reshape(horse, size(horse,1) * size(horse,2),3);
lab = rgb2lab(horse);
lab = reshape(lab(:,:,2:3), size(lab,1) * size(lab,2),2);

featureSpace = {double(intensities), double(rgb), double(lab)};

BICResults = cell(size(featureSpace,2), 8);

% Iterate over feature spaces
parfor featureNumber = 1 : size(featureSpace,2)
    display(['Running for feature number ', num2str(featureNumber)]);
    X = featureSpace{featureNumber};
    % Iterate over # of Gaussians
    for K = 2 : 8
        
        % Step 1: Fit model
        tic;
        
        % Pick 2 random rows (if multidimensional) as our mean
        u = datasample(X,K);
        
        % Set sigma to be one for all distributions
        sigma = ones(K, size(X,2));
        
        % Set alpha to be evenly divided among all gaussians
        alpha = repmat(1/K, [ K 1 ]);
        
        % Track previous mean/sigma
        u_prev = zeros(size(u));
        sigma_prev = zeros(size(sigma));
        
        % Compute normal distribution and store for faster calculation
        gaussian = zeros(size(X,1),K);
        % Probabilities for each x_i
        result = zeros(size(X,1),K);
        
        cont = 1;
        
        while(cont)
            
            % E step
            
            % First calculate the normal distribution and save it
            for k = 1 : K
                gaussian(:,k) = mvnpdf(X, u(k,:), sigma(k,:)) * alpha(k);
            end
            
            
            % Update probabilities
            for label = 1 : K
                result(:,label) = gaussian(:,label)./(sum(gaussian,2)+eps);
            end
            
            % M step
            for label = 1 : K
                tempSum = sum(result(:,label));
                alpha(label) = tempSum/size(X,1);
                sigma_prev = sigma;
                sigma(label,:) = (sum(bsxfun(@times,(bsxfun(@minus,X,u(label,:))).^2 , result(:,label))) / tempSum) + eps;
                u_prev = u;
                u(label,:) = sum(bsxfun(@times, X, result(:,label))) / tempSum;
            end
            
            % Check for convergence
            if (sum(abs(u(:) - u_prev(:))) < 0.01 && sum(abs(sigma(:) - sigma_prev(:))) < 0.01)
                cont = 0;
            end
        end
        
        % --- Model is now fit
        % Calculate p(x | theta,model)
        % Bases on slide #29 and the formula given in the assignment
        scoresPerPixels = zeros(size(X,1),1);
        for k = 1 : K
            scoresPerPixels = scoresPerPixels + mvnpdf(X, u(k,:), sigma(k,:)) * alpha(k);
        end
        
        n = size(X,1);
        k = numel(u) + numel(sigma) + numel(alpha);
        
        % Calculate, store BIC
        BIC = -2 * sum(log(scoresPerPixels)) + k * log(n);
        
        BICResults{featureNumber, K} = BIC;
        
        display(['Completed for K = ' , num2str(K), ' in ', num2str(toc)]);
    end
    
    display(['Completed for feature number ', num2str(featureNumber)]);
    % End iteration over different # of gaussians
    
end
% End iteration over different feature spaces

save('q2b');
