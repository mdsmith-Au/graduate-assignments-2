% Use this file to calculate/save results for Q1
% I could have factored this into functions but this is academia no?
directory = 'Q1_results';

close all;

mkdir(directory);

horse = imread('horse.jpg');

res1 = A1_Q1('intensity', 2, horse);
set(gca,'XTick',[]);
set(gca,'YTick',[]);
set(gca,'Position',[0 0 1 1]);
saveas(gca, [directory, '/', 'K2_intensity'],'png');
set(gca,'XTick',[]);
set(gca,'YTick',[]);
set(gca,'Position',[0 0 1 1]);
res2 = A1_Q1('intensity', 4, horse);
saveas(gca, [directory, '/', 'K4_intensity'],'png');
set(gca,'XTick',[]);
set(gca,'YTick',[]);
set(gca,'Position',[0 0 1 1]);
res3 = A1_Q1('intensity', 6, horse);
saveas(gca, [directory, '/', 'K6_intensity'],'png');
set(gca,'XTick',[]);
set(gca,'YTick',[]);
set(gca,'Position',[0 0 1 1]);

res4 = A1_Q1('rgb', 2, horse);
saveas(gca, [directory, '/', 'K2_rgb'],'png');
set(gca,'XTick',[]);
set(gca,'YTick',[]);
set(gca,'Position',[0 0 1 1]);
res5 = A1_Q1('rgb', 4, horse);
saveas(gca, [directory, '/', 'K4_rgb'],'png');
set(gca,'XTick',[]);
set(gca,'YTick',[]);
set(gca,'Position',[0 0 1 1]);
res6 = A1_Q1('rgb', 6, horse);
saveas(gca, [directory, '/', 'K6_rgb'],'png');
set(gca,'XTick',[]);
set(gca,'YTick',[]);
set(gca,'Position',[0 0 1 1]);

res7 = A1_Q1('lab', 2, horse);
saveas(gca, [directory, '/', 'K2_lab'],'png');
set(gca,'XTick',[]);
set(gca,'YTick',[]);
set(gca,'Position',[0 0 1 1]);
res8 = A1_Q1('lab', 4, horse);
saveas(gca, [directory, '/', 'K4_lab'],'png');
set(gca,'XTick',[]);
set(gca,'YTick',[]);
set(gca,'Position',[0 0 1 1]);
res9 = A1_Q1('lab', 6, horse);
saveas(gca, [directory, '/', 'K6_lab'],'png');
set(gca,'XTick',[]);
set(gca,'YTick',[]);
set(gca,'Position',[0 0 1 1]);

