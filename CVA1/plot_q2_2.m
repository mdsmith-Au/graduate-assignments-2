% Plots results for Q2.2
clear all;
load('q2b');

close all;

figure;

BICResults = cell2mat(BICResults);

plot(2:8,BICResults(1,:));
xlabel('Number of Gaussians');
ylabel('BIC');
title('Intensity');

figure;
plot(2:8,BICResults(2,:));
xlabel('Number of Gaussians');
ylabel('BIC');
title('RGB');

figure;
plot(2:8,BICResults(3,:));
xlabel('Number of Gaussians');
ylabel('BIC');
title('L*a*b');