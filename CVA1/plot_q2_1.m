% Plot results for Q2.1
clear all;
load('q2');

close all;

figure;

plot(2:8,featureSpaceScores{1});
xlabel('Number of Gaussians');
ylabel('Log-likelihood');
title('Intensity');

figure;
plot(2:8,featureSpaceScores{2});
xlabel('Number of Gaussians');
ylabel('Log-likelihood');
title('RGB');

figure;
plot(2:8,featureSpaceScores{3});
xlabel('Number of Gaussians');
ylabel('Log-likelihood');
title('L*a*b');