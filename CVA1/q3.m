%Q3
close all;
clear all;

%% 1. Set the relative path to the dataset main directory
% assume that the Weizmann dataset folder is in the same directory as this code
DBpath = './Weizmann dataset';

%% 2. Set SysType to 'win' or 'unix', based on your OS
SysType = 'unix';
l=dir(DBpath);
switch lower(SysType)
    case 'win'
        Sep='\';
    case 'unix'
        Sep='/';
    otherwise
        Sep='\';
end;

%% Load image names
Lpath = load(strcat(DBpath,Sep,'img_list.mat'));
Lpath = Lpath.fls;

%% Set the num of clusters
numClusters = 2;

%% 3. For each image, do
for i=1:length(Lpath)
    
    %% load the image
    imgPath = strcat(DBpath,Sep,Lpath(i).name,Sep,'src_color',Sep,Lpath(i).name,'.png');
    I = imread(imgPath);
    
    % Get file name
    [~, fileName, ~] = fileparts(imgPath);
    
    % image size
    [nrows, ncols, nchannels] = size(I);
    
    % Define feature spaces
    I_g = rgb2gray(I);

    intensities = reshape(I_g, [size(I_g,1)*size(I_g,2), 1]);
    rgb = reshape(I, size(I,1) * size(I,2),3);
    lab = rgb2lab(I);
    lab = reshape(lab(:,:,2:3), size(lab,1) * size(lab,2),2);
    
    
    %% perform K-means clustering in intensity feature space
    % assume that the result is in binaryMap variable
    binaryMap = kmeans(double(intensities), numClusters, 'Options',statset('UseParallel',1));
    
    
    %% Store the segmentation results
    binaryMap = reshape(binaryMap, size(I_g)) - 1;
    
    % Set the output folder name
    SegResultsSubPath = 'kmeans_intensity';
    
    % Create the directory (if it doesn't exist)
    outputDir = strcat(DBpath,Sep,fileName,Sep,SegResultsSubPath);
    mkdir(outputDir);
    
    % Store the image
    outputImagePath = strcat(DBpath,Sep,fileName,Sep,SegResultsSubPath,Sep,fileName,'.png');
    imwrite(binaryMap, outputImagePath);
    
    
    %% perform K-means clustering in RGB feature space
    binaryMap = kmeans(double(rgb), numClusters, 'Options',statset('UseParallel',1));
    
    
    %% Store the segmentation results
    SegResultsSubPath = 'kmeans_RGB';
    
    binaryMap = reshape(binaryMap, size(I_g)) - 1;
    
    outputDir = strcat(DBpath,Sep,fileName,Sep,SegResultsSubPath);
    mkdir(outputDir);
    
    % Store the image
    outputImagePath = strcat(DBpath,Sep,fileName,Sep,SegResultsSubPath,Sep,fileName,'.png');
    imwrite(binaryMap, outputImagePath);
    
    %% perform K-means clustering in Lab feature space
    binaryMap = kmeans(double(lab), numClusters, 'Options',statset('UseParallel',1));
    
    
    %% Store the segmentation results
    SegResultsSubPath = 'kmeans_LAB';
    
    binaryMap = reshape(binaryMap, size(I_g)) - 1;
    
    outputDir = strcat(DBpath,Sep,fileName,Sep,SegResultsSubPath);
    mkdir(outputDir);
    
    % Store the image
    outputImagePath = strcat(DBpath,Sep,fileName,Sep,SegResultsSubPath,Sep,fileName,'.png');
    imwrite(binaryMap, outputImagePath);
    
    %% GMM in intensity
    SegResultsSubPath = 'GMM_intensity';
    
    outputDir = strcat(DBpath,Sep,fileName,Sep,SegResultsSubPath);
    mkdir(outputDir);
    
    binaryMap = A1_Q1('intensity', numClusters, I) - 1;
    
    % Store the image
    outputImagePath = strcat(DBpath,Sep,fileName,Sep,SegResultsSubPath,Sep,fileName,'.png');
    imwrite(binaryMap, outputImagePath);
    
    %% GMM, RGB
   
    SegResultsSubPath = 'GMM_RGB';
    
    outputDir = strcat(DBpath,Sep,fileName,Sep,SegResultsSubPath);
    mkdir(outputDir);
    
    binaryMap = A1_Q1('rgb', numClusters, I) - 1;
    
    % Store the image
    outputImagePath = strcat(DBpath,Sep,fileName,Sep,SegResultsSubPath,Sep,fileName,'.png');
    imwrite(binaryMap, outputImagePath);
    
    %% GMM, LAB
   
    SegResultsSubPath = 'GMM_LAB';
    
    outputDir = strcat(DBpath,Sep,fileName,Sep,SegResultsSubPath);
    mkdir(outputDir);
    
    binaryMap = A1_Q1('lab', numClusters, I) - 1;
    
    % Store the image
    outputImagePath = strcat(DBpath,Sep,fileName,Sep,SegResultsSubPath,Sep,fileName,'.png');
    imwrite(binaryMap, outputImagePath);
end

%% Evaluate the performance scores
% K-means in intensity space
scores_kmeansIntens = ComputeFMeasure(DBpath, 'kmeans_intensity', SysType);
% K-means in RGB space
scores_kmeansRGB = ComputeFMeasure(DBpath, 'kmeans_RGB', SysType);
% K-means in Lab space
scores_kmeansLAB = ComputeFMeasure(DBpath, 'kmeans_LAB', SysType);
% GMM, intensity
scores_gmmIntens = ComputeFMeasure(DBpath, 'GMM_intensity', SysType);
%GMM, RGB
scores_gmmRGB= ComputeFMeasure(DBpath, 'GMM_RGB', SysType);
%GMM, LAB
scores_gmmLAB = ComputeFMeasure(DBpath, 'GMM_LAB', SysType);

save('q3');