% Q1
function [mapping] = A1_Q1(method, K, horse)

% Max # of iterations
maxIter = 1000;

horse_g = rgb2gray(horse);

% Execute appropriate method for feature space
if (strcmp(method, 'intensity'))
    intensities = reshape(horse_g, [size(horse_g,1)*size(horse_g,2), 1]);
    X = double(intensities);
    
elseif (strcmp(method, 'rgb'))
    rgb = reshape(horse, size(horse,1) * size(horse,2),3);
    X = double(rgb);
    
elseif (strcmp(method,'lab'))
    
    lab = rgb2lab(horse);
    lab = reshape(lab(:,:,2:3), size(lab,1) * size(lab,2),2);
    X = double(lab);
end

% Pick 2 random rows (if multidimensional) as our mean
u = datasample(X,K);

% Set sigma to be one for all distributions
sigma = ones(K, size(X,2));

% Set alpha to be evenly divided among all gaussians
alpha = repmat(1/K, [ K 1 ]);

% Track previous mean/sigma
u_prev = zeros(size(u));
sigma_prev = zeros(size(sigma));

% Compute normal distribution and store for faster calculation
gaussian = zeros(size(X,1),K);
% Probabilities for each x_i
result = zeros(size(X,1),K);

%y = zeros(size(result));

cont = 1;

iterationNumber = 0;

time = 0;
while(cont)

    tic;
    
    % E step
    
    % First calculate the normal distribution and save it
    for k = 1 : K
        gaussian(:,k) = mvnpdf(X, u(k,:), sigma(k,:)) * alpha(k);
    end
    
    
    % Update probabilities (posterior over the labels)
    for label = 1 : K
        result(:,label) = gaussian(:,label)./(sum(gaussian,2)+eps);
    end

    % Update!
    %y(:) = 0;
    %linInd = sub2ind(size(y), (1 : size(y,1))', I);
    %y(linInd) = 1;
    
    % M step
    for label = 1 : K
       tempSum = sum(result(:,label));
       alpha(label) = tempSum/size(X,1); 
       sigma_prev = sigma;
       sigma(label,:) = (sum(bsxfun(@times,(bsxfun(@minus,X,u(label,:))).^2 , result(:,label))) / tempSum) + eps;
       u_prev = u;
       u(label,:) = sum(bsxfun(@times, X, result(:,label))) / tempSum;
    end
    
    % Check for convergence
    if (sum(abs(u(:) - u_prev(:))) < 0.01 && sum(abs(sigma(:) - sigma_prev(:))) < 0.01)
        cont = 0;
    end
    
    iterationNumber = iterationNumber + 1;
    
    if (iterationNumber > maxIter)
        cont = 0;
        display('Warning: reached maximum iteration limit');
    end
    
    time = time + toc;
end

display(['Time per iteration = ', num2str(time/iterationNumber)]);

% Use likelihood (rather than posterior over the labels) for classification
% as per discussion with Siavash
gaussian = zeros(size(X,1),K);
result = zeros(size(X,1),K);

for k = 1 : K
    gaussian(:,k) = mvnpdf(X, u(k,:), sigma(k,:));
end

for label = 1 : K
    result(:,label) = gaussian(:,label)./(sum(gaussian,2)+eps);
end

[~,I] = max(result,[],2);
mapping = reshape(I, size(horse_g));
imagesc(mapping);
