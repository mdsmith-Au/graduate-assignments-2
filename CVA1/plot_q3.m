% Displays performance metric information for q3
load('q3');

display('Intensity:');
display(['K-means avg: ', num2str(mean(scores_kmeansIntens(:,1)))]);
display(['GMM avg: ', num2str(mean(scores_gmmIntens(:,1)))]);

display('RGB:');
display(['K-means avg: ', num2str(mean(scores_kmeansRGB(:,1)))]);
display(['GMM avg: ', num2str(mean(scores_gmmRGB(:,1)))]);


display('L*a*b:');
display(['K-means avg: ', num2str(mean(scores_kmeansLAB(:,1)))]);
display(['GMM avg: ', num2str(mean(scores_gmmLAB(:,1)))]);