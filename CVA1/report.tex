\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[margin=1in]{geometry}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{booktabs}

\begin{document}
\title{ECSE 626 Assignment 1 Report}
\date{\today}
\author{Michael Smith}
\maketitle

\section{Question 1}
\paragraph{}
Fig. \ref{twoG} shows segmentation results with 2 Gaussians. We see that all algorithms succeed in segmenting the horse from the background.  That said, all results include the horse's shadow and some extra bits of landscape that ideally should not have been grouped with the horse.  The L*a*b segmentation in particular is the only one that recovers the entire horse body (the others misclassify some parts) and despite the inclusion of the fence and shadow appears to be one of the best segmentations.

\begin{figure}[!htb]
\minipage{0.32\textwidth}
  \includegraphics[width=\linewidth]{Q1_results/K2_intensity.png}
\endminipage\hfill
\minipage{0.32\textwidth}
  \includegraphics[width=\linewidth]{Q1_results/K2_rgb.png}
\endminipage\hfill
\minipage{0.32\textwidth}%
  \includegraphics[width=\linewidth]{Q1_results/K2_lab.png}
\endminipage
\caption{Two Gaussians.  From left to right: results for the intensity, RGB and L*a*b feature spaces.}
\label{twoG}
\end{figure}

\paragraph{}
With 4 Gaussians as shown in Fig. \ref{fourG}, both the intensity and RGB feature spaces become extremely noisy although once again all algorithms at least segment along somewhat understandable boundaries, such as the horse's shadow and mane, the body and dark or light patches of terrain.  The L*a*b algorithm continues to demonstrate its superiority to the others as it produces a very clean and visually appealing result. 
\begin{figure}[!htb]
\minipage{0.32\textwidth}
  \includegraphics[width=\linewidth]{Q1_results/K4_intensity.png}
\endminipage\hfill
\minipage{0.32\textwidth}
  \includegraphics[width=\linewidth]{Q1_results/K4_rgb.png}
\endminipage\hfill
\minipage{0.32\textwidth}%
  \includegraphics[width=\linewidth]{Q1_results/K4_lab.png}
\endminipage
\caption{Four Gaussians.  From left to right: results for the intensity, RGB and L*a*b feature spaces.}
\label{fourG}
\end{figure}

\paragraph{}
With 6 Gaussians, shown in Fig. \ref{sixG}, the results become quite difficult to interpret.  Both intensity and RGB spaces lead to segmentations that are very noisy, with the intensity space being the worst as the horse is segmented into four different parts none of which are truly contiguous.  The RGB and L*a*b spaces produce better results as the horse is split into two main components.  Once again, however, the L*a*b space is very clean and thus the best segmentation.
\begin{figure}[!htb]
\minipage{0.32\textwidth}
  \includegraphics[width=\linewidth]{Q1_results/K6_intensity.png}
\endminipage\hfill
\minipage{0.32\textwidth}
  \includegraphics[width=\linewidth]{Q1_results/K6_rgb.png}
\endminipage\hfill
\minipage{0.32\textwidth}%
  \includegraphics[width=\linewidth]{Q1_results/K6_lab.png}
\endminipage
\caption{Six Gaussians.  From left to right: results for the intensity, RGB and L*a*b feature spaces.}
\label{sixG}
\end{figure}

\paragraph{}
Overall, the L*a*b space is the clear winner here although in practice none of the nine results would be considered acceptable in a production environment.  A segmentation algorithm that took into account spatial information about each pixel would likely perform much better.
\section{Question 2.1}
\begin{figure}[!htb]
\minipage{0.32\textwidth}
  \includegraphics[width=\linewidth]{q2_plots/intensity.png}
\endminipage\hfill
\minipage{0.32\textwidth}
  \includegraphics[width=\linewidth]{q2_plots/rgb.png}
\endminipage\hfill
\minipage{0.32\textwidth}%
  \includegraphics[width=\linewidth]{q2_plots/lab.png}
\endminipage
\caption{Plots showing the log likelihood as a function of the number of Gaussians.  From left to right: results for the intensity, RGB and L*a*b feature spaces.}
\label{q2_1}
\end{figure}
\paragraph{}
For this question, we see a general trend where a larger number of Gaussians are considered preferable based on the likelihood.  The intensity space shows a peak at 6 Gaussians, although in general anything between 3 through 6 Gaussians is acceptable.  The RGB feature space shows a general linear increase, which likely indicates a form of `overfitting' - an increase in the number of Gaussians means the data can be better represented although intuitively segmenting the image into anything more than half a dozen parts makes little sense. The L*a*b results show a peak at 4 Gaussians, with any additional segments not resulting in any significant increase in likelihood.  Overall, we can say that the RGB space promotes a variant of overfitting while the other two feature spaces prefer segmentations using anything between 4 to 6 Gaussians.

\section{Question 2.2}
\begin{figure}[!htb]
\minipage{0.32\textwidth}
  \includegraphics[width=\linewidth]{q2_plots2/intensity.png}
\endminipage\hfill
\minipage{0.32\textwidth}
  \includegraphics[width=\linewidth]{q2_plots2/rgb.png}
\endminipage\hfill
\minipage{0.32\textwidth}%
  \includegraphics[width=\linewidth]{q2_plots2/lab.png}
\endminipage
\caption{Plots showing the BIC as a function of the number of Gaussians.  From left to right: results for the intensity, RGB and L*a*b feature spaces.}
\label{q2_2}
\end{figure}
\paragraph{}
Applying the BIC formula as described in Eq. 2 of the assignment, we see that the best result can be found using 6 Gaussians in the intensity space, exactly as in the previous part.  The RGB space shows a similar `overfitting' trend as before, with a more or less linear decrease as the number of Gaussians increases.  The L*a*b space has a local minimum around 4 or 5 Gaussians with a general decrease as the number of Gaussians increases.  In all, the BIC results for each feature space mimic the results previously found in cross-validation, with each graph following the same pattern as before taking into account that we are looking for a minimum rather than a maximum.

\paragraph{}
Keeping in mind that the results from both metrics were similar, we can safely say that both algorithms are suitable for this problem.  In practice, however, the BIC would be preferable as it needs significantly fewer iterations to run than cross-validation and can thus be computed much faster.  If we were to tackle a different problem, though, the BIC might not fare as well as it relies on a complexity penalty that may have to be adjusted to suit a given problem whereas cross validation would require no such change as it depends only on the data.
\section{Question 3}
\begin{table}[htbp]
	\centering
	\caption{Average F-measure comparison}
	\label{part3}
  \begin{tabular}{lllr}
    \toprule
    & \multicolumn{3}{c}{Feature Space} \\
    \cmidrule(l){2-4}
    Method & Intensity    & RGB & L*a*b \\
    \midrule
    K-means  & 0.60 & 0.62 & 0.65 \\
    GMM      & 0.60 & 0.58 & 0.63 \\
    \bottomrule
  \end{tabular}
\end{table}

\paragraph{}
The F-Measure results, shown in Table \ref{part3} above, demonstrate the superiority of the L*a*b feature space when compared to either intensity or RGB.  This confirms the results seen in question 1 earlier.  With regards to the method, we can see that K-means performs the same or slightly better than the equivalent GMM implementation, with K-means in the L*a*b feature space producing the best results.

\paragraph{}
The reason for the poor performance of the EM GMM implementation can be explained by the fact that as implemented the EM model only makes use of a diagonal covariance matrix (as opposed to a full covariance matrix) meaning that it is restricted in its ability to represent the data.  A change to the EM implementation to allow the entire covariance matrix to be used would likely improve results and bring them in line with those produced by K-means.





%\bibliographystyle{plain}
%\bibliography{biblio}
\end{document}