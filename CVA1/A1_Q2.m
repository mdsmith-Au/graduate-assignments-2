function A1_Q2 ()
% Q2.1
horse = imread('horse.jpg');
horse_g = rgb2gray(horse);

% Load data, define feature spaces
intensities = reshape(horse_g, [size(horse_g,1)*size(horse_g,2), 1]);
rgb = reshape(horse, size(horse,1) * size(horse,2),3);
lab = rgb2lab(horse);
lab = reshape(lab(:,:,2:3), size(lab,1) * size(lab,2),2);

featureSpace = {double(intensities), double(rgb), double(lab)};

featureSpaceScores = cell(size(featureSpace));

% Iterate over feature spaces
parfor featureNumber = 1 : size(featureSpace,2)
    scores = [];
    X = featureSpace{featureNumber};
    % Iterate over # of Gaussians
    for K = 2 : 8
        
        modelScores = [];
        
        % Split data into 5 partitions
        kfolds = cell(1,5);
        % I know this throws a warning but it works...
        for kfold = 1 : 5
            kfolds{kfold} = X(  (kfold - 1) * size(X,1)/5 + 1 : kfold * size(X,1)/5,:);
        end
        
        % For k-fold...
        setofK = 1 : 5;
        for kfold = 1 : 5
            tic;
            % Determine testing/training data portions
            trainingIdx = setofK(setofK~=kfold);
            train = vertcat(kfolds{trainingIdx(1)},kfolds{trainingIdx(2)}, kfolds{trainingIdx(3)}, kfolds{trainingIdx(4)});
            
            test = kfolds{kfold};
            
            % ----- Fit model
            
            % Pick 2 random rows (if multidimensional) as our mean
            u = datasample(train,K);
            
            % Set sigma to be one for all distributions
            sigma = ones(K, size(train,2));
            
            % Set alpha to be evenly divided among all gaussians
            alpha = repmat(1/K, [ K 1 ]);
            
            % Track previous mean/sigma
            u_prev = zeros(size(u));
            sigma_prev = zeros(size(sigma));
            
            % Compute normal distribution and store for faster calculation
            gaussian = zeros(size(train,1),K);
            % Probabilities for each x_i
            result = zeros(size(train,1),K);
            
            cont = 1;
            
            while(cont)
                
                % E step
                
                % First calculate the normal distribution and save it
                for k = 1 : K
                    gaussian(:,k) = mvnpdf(train, u(k,:), sigma(k,:)) * alpha(k);
                end
                
                
                % Update probabilities
                for label = 1 : K
                    result(:,label) = gaussian(:,label)./(sum(gaussian,2)+eps);
                end
                
                % M step
                for label = 1 : K
                    tempSum = sum(result(:,label));
                    alpha(label) = tempSum/size(train,1);
                    sigma_prev = sigma;
                    sigma(label,:) = sum(bsxfun(@times,(bsxfun(@minus,train,u(label,:))).^2 , result(:,label))) / tempSum;
                    u_prev = u;
                    u(label,:) = sum(bsxfun(@times, train, result(:,label))) / tempSum;
                end
                
                %  Check for convergence
                if (sum(abs(u(:) - u_prev(:))) < 0.01 && sum(abs(sigma(:) - sigma_prev(:))) < 0.01)
                    cont = 0;
                end
            end
            
            % --- Model is now fit
            % Compute max likelihood as per slide #29 of EM (or more like
            % slide #33) and discussion with Siavash
            scoresPerPixels = zeros(size(test,1),1);
            for k = 1 : K
                 scoresPerPixels = scoresPerPixels + mvnpdf(test, u(k,:), sigma(k,:)) * alpha(k);
            end
            
            % Use log trick, which in combination with the mean as written in
            % the pseudo code lets us bypass calculating the product
            scoresPerPixels = log(scoresPerPixels);
            
            %------ Old version  --------
            % Reset following two variables
%             gaussian = zeros(size(test,1),K);
%             result = zeros(size(test,1),K);
%             
%             % Compute log likelihood of model with new test data
%             for k = 1 : K
%                 gaussian(:,k) = mvnpdf(test, u(k,:), sigma(k,:)) * alpha(k);
%             end
%             
%             
%             % Update probabilities
%             for label = 1 : K
%                 result(:,label) = gaussian(:,label)./(sum(gaussian,2)+eps);
%             end
%             
%             [~,I] = max(result,[],2);
%             linInd = sub2ind(size(result), (1 : size(result,1))', I);
            
            % Result variable now has log likelihood for model with train model
            % and test data; access it via index for the correct gaussian
            
%             scoresPerPixels = result(linInd);
%           -------
            
            % Save model score
            modelScores = [modelScores; mean(scoresPerPixels)];
            toc;
        end
        % End k-fold loop
        
        scores = [scores; mean(modelScores)];
        display(['Completed for K = ' , num2str(K)]);
    end
    display(['Completed for feature number ', num2str(featureNumber)]);
    % End iteration over different # of gaussians
    featureSpaceScores{featureNumber} =  scores;
end
% End iteration over different feature spaces

save('q2');
